{% extends "flutura-theme/theme.html" %}
{% block main-container %}
<div class="large-6 medium-6 small-6 columns no-padding pad-right wrap">
	<div class="kpi full blue"></div>
	<div class="large-6 medium-6 small-6 columns no-padding pad-right wrap">
		<div class="kpi full green"></div>
		<div class="kpi full red"></div>
	</div>
	<div class="large-6 medium-6 small-6 columns no-padding wrap">
		<div class="kpi full yellow"></div>
		<div class="kpi full orange"></div>
	</div>
</div>
<div class="large-6 medium-6 small-6 columns no-padding wrap">
	<div class="large-12 medium-12 small-12 container">
		<div class="header">
			<div class="icons columns">
				<ul class="inline-list right">
					<li><i class="fa fa-search"></i></li>
					<li><i class="fa fa-arrows-alt"></i></li>
					<li><i class="fa fa-download"></i></li>
					<li><i class="fa fa-chevron-down"></i></li>
				</ul>
			</div>
			<div class="title columns">Customer List</div>
		</div>
		<div class="body">
			<div class="columns">Body</div>
		</div>
	</div>
</div>
<div class="large-12 medium-12 small-12 container columns no-padding ">
	<div class="header">
		<div class="icons columns">
			<ul class="inline-list right">
				<li><i class="fa fa-search"></i></li>
				<li><i class="fa fa-arrows-alt"></i></li>
				<li><i class="fa fa-download"></i></li>
				<li><i class="fa fa-chevron-down"></i></li>
			</ul>
		</div>
		<div class="title columns">Customer List</div>
	</div>
	<div class="body">
		<div class="columns">
		<table>
			<thead>
				<tr><th>Abcedesss</th><th>Abcedesss</th><th>Abcedesss</th><th>Abcedesss</th><th>Abcedesss</th></tr>
			</thead>
			<tbody>
				<tr><td class="cust-info" link="customer-details"><a href="customerDetails?id=1234566789">1234566789</a></td><td>Abdcdhjjdl</td><td>skjascjkcjk</td><td>1111</td><td>1234.66</td></tr>
				<tr><td class="cust-info" link="customer-details"><a href="customerDetails?id=1234566789">1234566789</a></td><td>Abdcdhjjdl</td><td>skjascjkcjk</td><td>1111</td><td>1234.66</td></tr>
				<tr><td class="cust-info" link="customer-details"><a href="customerDetails?id=1234566789">1234566789</a></td><td>Abdcdhjjdl</td><td>skjascjkcjk</td><td>1111</td><td>1234.66</td></tr>
				<tr><td class="cust-info" link="customer-details"><a href="customerDetails?id=1234566789">1234566789</a></td><td>Abdcdhjjdl</td><td>skjascjkcjk</td><td>1111</td><td>1234.66</td></tr>
				<tr><td class="cust-info" link="customer-details"><a href="customerDetails?id=1234566789">1234566789</a></td><td>Abdcdhjjdl</td><td>skjascjkcjk</td><td>1111</td><td>1234.66</td></tr>
			</tbody>
		</table>
		</div>
	</div>
</div>
<script>
	$(".cust-info").click(function(){
		if($(this).attr("link") != "")
			$(".main-container").load($(this).attr("link") + ".html");
	});
</script>
{% endblock %}