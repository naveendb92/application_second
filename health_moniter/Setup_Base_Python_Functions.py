__author__ = 'flusr.in0072'

import pandas as pd
import psycopg2
import numpy as np
import datetime as dt

#import connectDB # this is simply a module that returns a connection to the db
class ReadFaker:
    """
    This could be extended to include the index column optionally. Right now the index
    is not inserted
    """
    def __init__(self, data):
        self.iter = data.itertuples()

    def readline(self, size=None):
        try:
            line = self.iter.next()[1:]  # element 0 is the index
            row = '\t'.join(x.encode('utf8') if isinstance(x, unicode) else str(x) for x in line) + '\n'
        # in my case all strings in line are unicode objects.
        except StopIteration:
            return ''
        else:
            return row

    read = readline

def insert(df, table, con=None, columns = None):

    time1 = dt.datetime.now()
    close_con = False
    if not con:
        try:
            conn_string = "host='localhost' dbname='Philips_temp' user='postgres' password='password' port='5555'"
            psycopg2.connect(conn_string)   ###dbLoader returns a connection with my settings
            close_con = True
        except psycopg2.Error, e:
            print e.pgerror
            print e.pgcode
            return "failed"
    inserted_rows = df.shape[0]
    data = ReadFaker(df)

    try:
        curs = con.cursor()
        print 'inserting %s entries into %s ...' % (inserted_rows, table)
        if columns is not None:
            curs.copy_from(data, table, null='nan', columns=[col for col in columns])
        else:
            curs.copy_from(data, table, null='nan')
        con.commit()
        curs.close()
        if close_con:
            con.close()
    except psycopg2.Error, e:
        print e.pgerror
        print e.pgcode
        con.rollback()
        if close_con:
            con.close()
        return "failed"

    time2 = dt.datetime.now()
    print time2 - time1
    return inserted_rows


def sql_execute(sql):
    import psycopg2
    conn_string = "host='localhost' dbname='Philips_temp' user='postgres' password='password' port='5555'"
    con = psycopg2.connect(conn_string)
    cursor = con.cursor()
    cursor.execute(sql)
    results=cursor.fetchall()
    cursor.close()
    return(results)

def sql_val_insert(sql):
    import psycopg2
    conn_string = "host='localhost' dbname='Philips_temp' user='postgres' password='password' port='5555'"
    con = psycopg2.connect(conn_string)
    cursor = con.cursor()
    cursor.execute(sql)
    cursor.close()
    return(0)


def sql_insert(df,table):
    import psycopg2
    conn_string = "host='localhost' dbname='Philips_temp' user='postgres' password='password' port='5555'"
    con = psycopg2.connect(conn_string)
    cursor = con.cursor()
    insert(df,table,con)
    cursor.close()
    results=1
    return(results)
#-----------------------------------------------------------------------------------------------

#Python codes to extract the data from API
#Weather channel : worldweatheronline.com
#API Credentials    : key=72db368510ed58d56e3800d34a7b4
#API Base Link      : "http://api.worldweatheronline.com/free/v2/weather.ashx"
#Location           : Malpe

#Step 1 : Construct the Dynamic API Call 
def weather_forecast(location,days):
    import urllib2 as url
    import datetime as dt 
    loc=location
    base_link="http://api.worldweatheronline.com/free/v2/weather.ashx"
    key="?key=72db368510ed58d56e3800d34a7b4"
    location="&q="+location
    format="&format=csv"
    days="&num_of_days=5"
    api_link=base_link+key+location+days+format

    #Extracting data through the API call
    txt = url.urlopen(api_link).read()
    aaa=txt.split('\n') #Converting the single line text to multiple lines

    weather_header=aaa[9] 
    weather_header=weather_header.replace('#','')
    weather_header=weather_header.split(',')

    astronomy_header=aaa[6]
    astronomy_header=astronomy_header.replace('#','')
    astronomy_header=astronomy_header.split(',')

    df=pd.DataFrame(aaa)
    df.columns=['Column']
    new=pd.DataFrame(df['Column'].str.split(',').tolist(),columns=['a1','a2','a3','a4','a5','a6','a7','a8','a9','a10','a11','a12','a13','a14','a15','a16','a17','a18','a19','a20','a21','a22','a23','a24','a25','a26','a27','a28','a29','a30','a31','a32','a33','a34','a35','a36'])

    #Creating the range for Astronomy Data
    b=range(12,len(new),9) 
    astronomy_range=b
    astronomy_range=astronomy_range[0:5]

    #Creating a range for Weather Data
    a=[]
    for x in range(12,len(new)-1,9):
        a=a+(range(x+1,x+9,1))
    weather_range=a

    #Creating Weather Data
    weather=new.ix[weather_range,:]
    weather.columns=weather_header
    weather['Location']=loc
    weather['forecasting_date']=str(dt.date.today())

    #Creating Astronomy Data
    astronomy=new.ix[astronomy_range,0:9]
    astronomy.columns=astronomy_header
    astronomy['Location']=loc
    astronomy['forecasting_date']=str(dt.date.today())

    #Consolidating the Astronomy and Weather Dataframes into a single list
    list=[astronomy,weather]
    return(list)
#------------------------------------------------------------------------------------------------

#Create a function to pull historic data
    #Once this is done, pump the data to Weather data
#------------------------------------------------------
#Call the function for 5 days of data [Astronomy Data + Weather Data]

def weather_historical(location,start_date,end_date):
    import urllib2 as url
    loc=location
    base_link="http://api.worldweatheronline.com/free/v2/past-weather.ashx"
    key="?key=72db368510ed58d56e3800d34a7b4"
    location = "&q=" + location
    format = "&format=csv"
    start_date="&date="+ start_date
    end_date = "&enddate=" + end_date
    tp = "&tp=3"
    api_link=base_link+key+location+start_date+end_date+tp+format

    #Extracting data through the API call
    txt = url.urlopen(api_link).read()
    aaa=txt.split('\n') #Converting the single line text to multiple lines
    weather_header=aaa[5] 
    weather_header=weather_header.replace('#','')
    weather_header=weather_header.split(',')

    astronomy_header=aaa[2]
    astronomy_header=astronomy_header.replace('#','')
    astronomy_header=astronomy_header.split(',')

    #Casting as a Dataframe for Pandas operations
    df=pd.DataFrame(aaa)
    df.columns=['Column']
    new=pd.DataFrame(df['Column'].str.split(',').tolist(),columns=['a1','a2','a3','a4','a5','a6','a7','a8','a9','a10','a11','a12','a13','a14','a15','a16','a17','a18','a19','a20','a21','a22','a23','a24','a25','a26'])

    #Creating the range for Astronomy Data
    b=range(8,len(new),9) 
    astronomy_range=b
    astronomy_range=astronomy_range[0:len(astronomy_range)-1]

    #Creating a range for Weather Data
    a=[]
    for x in range(8,len(new)-1,9):
        a=a+(range(x+1,x+9,1))
    weather_range=a

    #Creating Weather Data
    weather=new.ix[weather_range,:]
    weather.columns=weather_header
    weather['Location']=loc
    #Creating Astronomy Data
    astronomy=new.ix[astronomy_range,0:9]
    astronomy.columns=astronomy_header
    astronomy['Location']=loc

    #Consolidating the Astronomy and Weather Dataframes into a single list
    list=[astronomy,weather]
    return(list)

