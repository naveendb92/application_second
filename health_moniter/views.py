from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.core import serializers
from django.template import RequestContext, loader
import json
from django.db import connection
import collections
import decimal
import datetime as dt
from django.core import serializers
import collections
from health_moniter.models import master
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import ensure_csrf_cookie
from health_moniter.Setup_Base_Python_Functions import *
from health_moniter.Part4 import *
import numpy as np

@csrf_exempt
@ensure_csrf_cookie

# Create your views here.
@csrf_exempt
def overview(request):
	template = loader.get_template('health_moniter/overview/overview.html')
	context = RequestContext(request, {'appuser': "admin"})
	return HttpResponse(template.render(context), content_type="text/html")	
	
@csrf_exempt
def log_overview(request):
	overview_date = request.GET.get('lgdate')
	overview_categry = request.GET.get('category')
	overview_paramname = request.GET.get('over_paramname')
	overview_logid = request.GET.get('over_logid')
	var1 = request.GET.get('probdate1')
	pname = request.GET.get('paramname')
	logid = request.GET.get('logid')
	category = request.GET.get('category')
	issue = request.GET.get('issue')
	template = loader.get_template('health_moniter/Log Overview/log_overview.html')
	context = RequestContext(request, {'appuser': "admin", "paramname":var1, "pname":pname, "overviewdate":overview_date, "logid":logid, "category":category, "issue":issue,"overview_categry":overview_categry,"overview_paramname":overview_paramname,"overview_logid":overview_logid })
	return HttpResponse(template.render(context), content_type="text/html")
	#return HttpResponse(var1, content_type="text/html")
	
@csrf_exempt
def generation(request):
	var1 = request.GET.get('probdate1')
	template = loader.get_template('health_moniter/generation/generation.html')
	context = RequestContext(request, {'appuser': "admin", "paramname":var1})
	return HttpResponse(template.render(context), content_type="text/html")	
	
@csrf_exempt
def consumption(request):
	template = loader.get_template('health_moniter/consumption/consumption.html')
	context = RequestContext(request, {'appuser': "admin"})	
	return HttpResponse(template.render(context), content_type="text/html")	

@csrf_exempt
def Site_Visit_Log(request):
	template = loader.get_template('health_moniter/Site Visit Log/Site_Visit_Log.html')
	context = RequestContext(request, {'appuser': "admin"})	
	return HttpResponse(template.render(context), content_type="text/html")	

def Create_Log(request):
	template = loader.get_template('health_moniter/Create Log/Create_Log.html')
	context = RequestContext(request, {'appuser': "admin"})	
	return HttpResponse(template.render(context), content_type="text/html")		

def Review_Log(request):
	template = loader.get_template('health_moniter/Review Log/Review_Log.html')
	context = RequestContext(request, {'appuser': "admin"})	
	return HttpResponse(template.render(context), content_type="text/html")	
	
def small_window(request):
	template = loader.get_template('health_moniter/storage/small_window.html')
	context = RequestContext(request, {'appuser': "admin"})
	return HttpResponse(template.render(context), content_type="text/html")		
	
loc = 'Malpe' 
date = str(dt.date.today())
date_time = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

def getrefreshtime(request):
	cursor = connection.cursor()
	time = "select max(time) from master where date(date) = date(now())"
	cursor.execute(time)
	time1 = cursor.fetchall()
	for row in time1:
		a = row[0]	
	return HttpResponse(a)	

def getbestworstcasedate(request):
	arg = request.GET.get('arg')
	crnt_timestamp=date_time
	location='Malpe'
	capacity=battery_capacity(loc,date_time)
	data=load_hourly_consumption(location,crnt_timestamp,5)
	new_result=final_battery_results11(data,5,  [1,1,1,1,1]  ,capacity)	
	if arg == 'wc':
		hour2=[]
		i=0
		for row in new_result[1]:
			d=[]
			d.append(float(row[-1]))
			hour2.append(d)
			i=i+1
		hour1 = i
	
	if arg == 'bc':
		hour2=[]
		i=0
		for row in new_result[0]:
			d=[]
			d.append(float(row[-1]))
			hour2.append(d)
			i=i+1
		hour1 = i	
			
	pagedata = {'total_count':hour1,'total_sum':hour2}
	return HttpResponse(json.dumps(pagedata), content_type = 'application/json')
	
def getcustom_case_result(request):
	num = request.GET.get('arg1')
	number=decimal.Decimal(num)
	crnt_timestamp=date_time
	location='Malpe'
	capacity=battery_capacity(loc,date_time)
	data=load_hourly_consumption(location,crnt_timestamp,5)	
	cust_res=custom_battery_results(data,6,[1,1,1,1,1],capacity,number)
	
	pagedata = {'cust_res':cust_res}
	return HttpResponse(json.dumps(pagedata), content_type = 'application/json')	
	
def getoverviewpagedata(request):
	cursor = connection.cursor()
	arg = request.GET.get('a')
	
	click_bar_date = request.GET.get('click_on_bar_date')
	
	if not click_bar_date:
		click_bar_date = '2016-02-10'
	
	if not arg:
		arg='Load'

	overview_list = "select * from system_alert_log where date(log_creation_time)='2016-03-04' and category = '"+arg+"' "	
		
	overview_color = "select * from ECOSYSTEM_OVERVIEW('2016-03-04')"
	
	solarpaneloutput = "select * from solarpanel_outut('"+loc+"','2016-02-22')"
	
	#sys_alert_log = "select a.*,to_char(time_of_Creation, 'YYYY-MM-DD HH24:MI:SS') as date  from system_alert_log a "
	sys_alert_log = "select * from system_alert_log limit 101"
	
	bat_cyc_eff_dummy = "select * from batterycycle_efficiency_dummy"
	
	sol_pan_op = "select a.*,to_char(date_time,'Mon-DD') from solar_panel_output a where date(date_time) between '2016-02-23' and '2016-02-28'"
	
	# gen_con_rate_dummy = "select * from gen_con_rate_dummy"
	#gen_con_rate = "select * from GENERATION_VS_CONSUMPTION('Malpe','2016-02-27 23:00:00')"
	gen_con_rate = "select a.*,hour||':00' as hourr from GENERATION_VS_CONSUMPTION('Malpe','2016-02-27 23:00:00') a"
	
	# log_graph_dummy = "select * from log_graph_dummy"
	log_graph_dummy = "select a.*, to_char(date, 'Mon-DD') as datee from LOG_STATISTICS('2016-02-29',10) a order by date asc"
	
	click_on_bar_graph = "select extract(hour from time_of_creation),count(*) from system_alert_log where date(time_of_creation)='"+click_bar_date+"' group by time_of_creation order by date_part asc"
	
	
	#Click on Bar Grapk:
	cursor.execute(click_on_bar_graph)
	click_on_bar_graphh = cursor.fetchall()
	bartime=[]
	barvalue=[]
	for row in click_on_bar_graphh:
		bartime.append(int(row[0]))
		barvalue.append(int(row[1]))
		
	
	#Overview-Color:
	cursor.execute(overview_color)
	overview_colorr = cursor.fetchall()
	over_category=[]
	over_logs=[]
	over_color=[]
	for row in overview_colorr:
		over_category.append(str(row[0]))
		over_logs.append(int(row[1]))
		over_color.append(str(row[2]))
		
	
	#Overview-List-dummy:
	cursor.execute(overview_list)
	overview_listt = cursor.fetchall()
	over_list=[]
	for row in overview_listt:
		d = collections.OrderedDict()
		d['time'] = str(row[2])  #0
		d['info'] = str(row[7])	 #7
		d['category'] = str(row[6])
		d['paramname'] = str(row[10])
		d['logid'] = str(row[1])
		over_list.append(d)
	
	#Log-Graph:
	cursor.execute(log_graph_dummy)
	log_graph_dummyy = cursor.fetchall()
	logdate=[]
	logcount=[]
	lognewcount=[]
	for row in log_graph_dummyy:
		logdate.append(str(row[3]))
		logcount.append(int(row[1]))
		lognewcount.append(int(row[2]))
	
	#Generation/Consumption Rate:
	cursor.execute(gen_con_rate)
	gen_con_rate_dummyy = cursor.fetchall()
	gctime=[]
	gen=[]
	con=[]
	for row in gen_con_rate_dummyy:
		gctime.append(str(row[5]))
		gen.append(float(row[3]))
		con.append(float(row[4]))
		
	
	#Solar-Panel-Output-Table:
	cursor.execute(sol_pan_op)
	sol_pan_opp = cursor.fetchall()
	dte=[]
	op=[]
	for row in sol_pan_opp:
		dte.append(str(row[3]))
		op.append(int(row[2]))	
	
	
	#Battery-Cycle-Efficiency-Dummy-Table:
	cursor.execute(bat_cyc_eff_dummy)
	bat_cyc_eff_dummyy = cursor.fetchall()
	date=[]
	eff=[]
	for row in bat_cyc_eff_dummyy:
		date.append(str(row[0]))
		eff.append(int(row[1]))
	

	#Solar-Panel-Output
	cursor.execute(solarpaneloutput)
	solarpaneloutput1 = cursor.fetchall()
	for row in solarpaneloutput1:
		solarpaneloutt = float(round(row[4],2))
		spv = float(round(row[2],2))

	
	#Systen-Alert-Log:
	time=[]
	prob=[]
	noofparams=[]
	paramname=[]
	logtype=[]
	logid=[]
	logcategory=[]
	cursor.execute(sys_alert_log)
	sys_alert_log1 = cursor.fetchall()
	for row in sys_alert_log1:
		time.append(str(row[2]))
		prob.append(str(row[7]))
		noofparams.append(int(row[8]))
		paramname.append(str(row[10]))
		logtype.append(str(row[5]))
		logid.append(str(row[1]))
		logcategory.append(str(row[6]))
		
					
	pagedata = {'time':time,'prob':prob,'noofparams':noofparams,'logid':logid,'paramname':paramname,'logtype':logtype,'logcategory':logcategory,'solarpaneloutput':solarpaneloutt,'spv':spv,'date':date,'eff':eff,'dte':dte,'op':op,'gctime':gctime,'gen':gen,'con':con,
	'logdate':logdate,'logcount':logcount,'lognewcount':lognewcount,'over_list':over_list,'over_category':over_category,'over_logs':over_logs,'over_color':over_color,'bartime':bartime,'barvalue':barvalue}	
	return HttpResponse(json.dumps(pagedata),content_type="application/json")
	
def gotopage2(request):
	cursor = connection.cursor()
	arg1 = request.GET.get('probdate1')
	
	paramname = "select parameter_name from log_parameter where log_id = '"+arg1+"'"    
	
	#Get the Parameter name:
	n=[]
	cursor.execute(paramname)
	name = cursor.fetchall()
	for row in name:
		n.append(str(row[0]))
	
	
	#template = loader.get_template('health_moniter/generation/generation.html')	
	#context = RequestContext(request, {'appuser': "admin", "paramname": n})
	return HttpResponse(json.dumps(n),content_type="application/json")
	#return HttpResponse(n, content_type="text/html")
	
def getlogoverviewdata(request):
	cursor = connection.cursor()
	param_table='solarpanel'

	
	sal_param_name = request.GET.get('paramname')
	
	a = request.GET.get('probdate1')
	
	b = request.GET.get('overviewdate')
	
	Categoryy = request.GET.get('overviewcategory')
	
	param_name = request.GET.get('overviewParamname')
	
	over_logid = request.GET.get('overviewLogid')
	
	logid = request.GET.get('logID')
	
	Categoryy = request.GET.get('category')
	
	Issuee = request.GET.get('issue')
	
	if over_logid != 'None':
		logid = over_logid
		
	if 	sal_param_name != 'None':
		param_name = sal_param_name
	
	
	if a == 'None':
		vector_date = b
	if a != 'None':
		vector_date = a
	
	if b == 'None':
		vector_date = a	

	if b != 'None':
		vector_date = b		
	
	#vector_param_date = vector_date

	

	if 	param_name == 'None':
		param_name = 'solarvoltage' 	
	if vector_date == 'None':
		vector_date = '2016-02-29'
		
	if param_name == 'solarpower':					#param_name
		param_table = 'solarpanel'					#param_table
		
	if not vector_date:
		vector_date = '2016-02-29'
	
	#loggraph = "select * from generation_rate where date='"+vector_date+"' order by hour"            #Vector Graph
	loggraph = "select a.*,hour||':00' as hr from generation_rate a where date='"+vector_date+"' order by hour "            #Vector Graph
	
	paramter_graph = "select cast(date_time as time),"+param_name+" from "+param_table+" where cast(date_time as date)='"+vector_date+"'"  #Parameter Graph
	
	recm_actns = "select * from recommended_actions where log_id='"+logid+"'"
	
	recm_count = "select count(*) from recommended_actions where log_id='"+logid+"'"
	
	log_overview_bar = "SELECT a.*,to_char(date,'Mon-DD') as dt FROM LOG_OVERVIEW_BAR('"+vector_date+"','"+Categoryy+"','"+Issuee+"') a order by dt"
	
	similar_events = "select * from LOG_OVERVIEW_SIMICOUNT('"+vector_date+"','"+Categoryy+"','"+Issuee+"')"
	
	#Recommendations-Count:
	cursor.execute(recm_count)
	recm_countt = cursor.fetchall()
	for row in recm_countt:
		recom_count = int(row[0])
	
	#Similar Events:
	cursor.execute(similar_events)
	similar_eventss = cursor.fetchall()
	for row in similar_eventss:
		similar_count = int(row[0])
	
	
	#Log-Overview-Bar-Graph:
	cursor.execute(log_overview_bar)
	log_overview_barr = cursor.fetchall()
	bardate=[]
	barcount=[]
	for row in log_overview_barr:
		bardate.append(str(row[2]))
		barcount.append(int(row[1]))
	
	
	
	#Recommended Actions:
	cursor.execute(recm_actns)
	recm_actnss = cursor.fetchall()
	logidd=[]
	actions = []
	for row in recm_actnss:
		logidd.append(str(row[0]))
		actions.append(str(row[1]))
		
	
	#Parameter Graph:
	cursor.execute(paramter_graph)
	paramter_graphh = cursor.fetchall()
	paramdate=[]
	paramcol=[]
	for row in paramter_graphh:
		paramdate.append(str(row[0]))
		paramcol.append(float(row[1]))
	
	#On Click Get Log Graph:
	cursor.execute(loggraph)
	loggraphh = cursor.fetchall()
	logtime=[]
	p1=[]
	p2=[]
	for row in loggraphh:
		logtime.append(str(row[6]))
		p1.append(float(row[3]))
		p2.append(float(row[5]))
		
		
	pagedata = {'logtime':logtime,'p1':p1,'p2':p2,'paramdate':paramdate,'paramcol':paramcol,'actions':actions,'logidd':logidd,'bardate':bardate,'barcount':barcount,'vector_date':vector_date,'Categoryy':Categoryy,'Issuee':Issuee,'similar_count':similar_count,'recom_count':recom_count}
	return HttpResponse(json.dumps(pagedata),content_type="application/json")			
	
	
def getgenerationpagedata(request):
	cursor = connection.cursor()
	arg1 = request.GET.get('arg1')
	arg2 = request.GET.get('arg2')
	arg3 = request.GET.get('arg3')
	date = request.GET.get('date')
	param1 = request.GET.get('param1')
	param2 = request.GET.get('param2')
	gensd = request.GET.get('gensd')
	gened = request.GET.get('gened')
	genavg = request.GET.get('genavg')
	amplifysolarparameter = request.GET.get('amplifysolarparameter')
	amplifysolarvalue = request.GET.get('amplifysolarvalue')
	
	if not amplifysolarparameter:
		amplifysolarparameter='SOLARVOLTAGE'
	if not amplifysolarvalue:
		amplifysolarvalue='1'
	if not genavg:
		genavg = 'avg'
	if not gensd:
		gensd = '2016-01-10'
	if not gened:
		gened = '2016-02-15'
	if not param2:
		param2 = 'SOLARVOLTAGE'
	if not param1:
		param1 = 'SOLARPOWER'
	if not arg3:
		arg3='2016-01-10'
	if not date:
		date='2016-01-25'
	if not arg1:
		arg1='SOLARVOLTAGE'
	if not arg2:
		arg2='SOLARCURRENT'
		
	page2 = "select * from PAGE2WITHDATE('"+arg1+"','"+arg2+"','"+arg3+"')"
	storage_battery = "select * from PAGE2WITHDATE('BATTERYVOLTAGE','BATTERYCURRENT','2016-01-25')"
	page2load = "select * from PAGE2WITHDATE('LOADVOLTAGE','LOADCURRENT','2016-01-15')"
	weekly_avg = "SELECT * FROM PAGE2WEEKLYAVG('"+gensd+"','"+gened+"','"+genavg+"','"+param1+"','"+param2+"')"
	
	#Weekly-Average:
	cursor.execute(weekly_avg)
	week_avg = cursor.fetchall()
	weekxaxis=[]
	weekvalues1=[]
	weekvalues2=[]
	for row in week_avg:
		weekxaxis.append(row[4])
		weekvalues2.append(round(float(row[3]),1))
		weekvalues1.append(round(float(row[2]),1))
	
	#pAGE 2 LOAD DATA:
	cursor.execute(page2load)
	p2load = cursor.fetchall()
	xaxisss=[]
	col111=[]
	col222=[]
	for row in p2load:
		xaxisss.append(str(row[0]))
		col111.append(float(row[1]))
		col222.append(float(row[2]))		
	
	#Page 2 storage-battery Query:
	cursor.execute(storage_battery)
	sb = cursor.fetchall()
	xaxiss=[]
	col11=[]
	col22=[]
	for row in sb:
		xaxiss.append(str(row[0]))
		col11.append(float(row[1]))
		col22.append(float(row[2]))		
	
	#Page 2 part-1 Query:
	cursor.execute(page2)
	page22 = cursor.fetchall()
	xaxis=[]
	col1=[]
	col2=[]	
	for row in page22:
		xaxis.append(str(row[0]))
		col1.append(round(float(row[1]),1))
		col2.append(round(float(row[2]),1))

	aa = np.amax(col1)  #173.5
	bb = np.amax(col2)	#89.5	
	
	nwcol1=[]
	newcol2=[];
	if aa > bb:
		for row in col2:	
			newcol2.append((float(row))*float(amplifysolarvalue))
	if aa < bb:
		for row in col1:
			nwcol1.append((float(row))*float(amplifysolarvalue))
			
	if not nwcol1:
		nwcol1 = col1
		
	if not newcol2:
		newcol2 = col2
		
	
	
	pagedata = {'xaxis':xaxis,'col1':nwcol1,'col2':newcol2,'xaxiss':xaxiss,'col11':col11,'col22':col22,'xaxisss':xaxisss,'col111':col111,'col222':col222,'weekxaxis':weekxaxis,'weekvalues1':weekvalues1,'weekvalues2':weekvalues2}	
	#pagedata = {'newcol1':nwcol1,'newcol2':newcol2}
	return HttpResponse(json.dumps(pagedata),content_type="application/json")	

def getstoragebatterypagedata(request):
	cursor = connection.cursor()
	arg1 = request.GET.get('arg1')
	arg2 = request.GET.get('arg2')
	arg3 = request.GET.get('arg3')
	sbd = request.GET.get('date')
	param1 = request.GET.get('param1')
	param2 = request.GET.get('param2')

	if not param1:
		param1='BATTERYVOLTAGE'
	if not param2:
		param2='BATTERYCURRENT'
	if not arg3:
		arg3='2016-01-10'
	if not sbd:
		sbd='2016-01-10'
	if not arg1:
		arg1='BATTERYVOLTAGE'
	if not arg2:
		arg2='BATTERYCURRENT'
	
	weekly_avg = "SELECT * FROM PAGE2WEEKLYAVG('2016-01-10','2016-01-15','min','"+param1+"','"+param2+"')"
	storage_battery = "select * from PAGE2WITHDATE('"+arg1+"','"+arg2+"','"+arg3+"')"
	
	
	#Weekly-Average:
	cursor.execute(weekly_avg)
	wk_avg = cursor.fetchall()
	wkxaxis=[]
	wkvalues1=[]
	wkvalues2=[]
	for row in wk_avg:
		wkxaxis.append(row[4])
		wkvalues2.append(float(row[3]))
		wkvalues1.append(float(row[2]))
	
	
	#Page 2 storage_battery Query:
	cursor.execute(storage_battery)
	page22 = cursor.fetchall()
	xaxis=[]
	col1=[]
	col2=[]
	for row in page22:
		xaxis.append(str(row[0]))
		col1.append(float(row[1]))
		col2.append(float(row[2]))
	

					
	pagedata = {'xaxis':xaxis,'col1':col1,'col2':col2,'wkxaxis':wkxaxis,'wkvalues1':wkvalues1,'wkvalues2':wkvalues2}	
	return HttpResponse(json.dumps(pagedata),content_type="application/json")	

def getstoragebatterypagedataweekly(request):
	cursor = connection.cursor()

	param1 = request.GET.get('param1')
	param2 = request.GET.get('param2')
	startdate = request.GET.get('sd')
	enddate = request.GET.get('ed')
	avg = request.GET.get('avg')
	
	if not avg:
		avg='avg'	
	if not enddate:
		enddate = '2016-02-14'
	if not startdate:
		startdate='2016-02-13'
	if not param1:
		param1='BATTERYVOLTAGE'
	if not param2:
		param2='BATTERYCURRENT'

	
	weekly_avg = "SELECT * FROM PAGE2WEEKLYAVG('"+startdate+"','"+enddate+"','"+avg+"','"+param1+"','"+param2+"')"
	
	
	
	#Weekly-Average:
	cursor.execute(weekly_avg)
	wk_avg = cursor.fetchall()
	wkxaxis=[]
	wkvalues1=[]
	wkvalues2=[]
	for row in wk_avg:
		wkxaxis.append(row[4])
		wkvalues2.append(round(float(row[3]),1))
		wkvalues1.append(round(float(row[2]),1))
	
				
	pagedata = {'wkxaxis':wkxaxis,'wkvalues1':wkvalues1,'wkvalues2':wkvalues2}	
	return HttpResponse(json.dumps(pagedata),content_type="application/json")	



	
def getpage2loaddata(request):
	cursor = connection.cursor()
	arg1 = request.GET.get('arg1')
	arg2 = request.GET.get('arg2')
	arg3 = request.GET.get('arg3')
	arg4 = request.GET.get('arg4')
	loadweeklycol1 = request.GET.get('col1')
	loadweeklycol2 = request.GET.get('col2')
	loadweeklysd = request.GET.get('startdate')
	loadweeklyed = request.GET.get('enddate')
	loadweeklyavg = request.GET.get('avg')
	type = request.GET.get('type')
	
	
	if not loadweeklyavg:
		loadweeklyavg='avg'
	if not loadweeklyed:
		loadweeklyed='2016-02-14'
	if not loadweeklysd:
		loadweeklysd='2016-01-13'
	if not loadweeklycol1:
		loadweeklycol1 = 'LOADVOLTAGE'
	if not loadweeklycol2:
		loadweeklycol2 = 'LOADCURRENT'
	if not type:
		type='Load 1'
	if not arg1:
		arg1='LOADVOLTAGE'
	if not arg2:
		arg2='LOADCURRENT'
	if not arg3:
		arg3='2016-01-10'
	if not 	arg4:
		arg4='Load 1'
	
	page2load = "select * from page2forload('"+arg1+"','"+arg2+"','"+arg3+"','"+arg4+"')"
	#page2load = "select * from PAGE2WITHDATE('"+arg1+"','"+arg2+"','"+arg3+"')"
	#weekly_avg = "SELECT * FROM PAGE2WEEKLYAVG('"+loadweeklysd+"','"+loadweeklyed+"','"+loadweeklyavg+"','"+loadweeklycol1+"','"+loadweeklycol2+"')"
	weekly_avg = "SELECT * FROM PAGE2LOAD('"+loadweeklysd+"','"+loadweeklyed+"','"+loadweeklyavg+"','"+loadweeklycol1+"','"+loadweeklycol2+"','"+type+"')"
	
	#Page @ load Weekly Average:
	cursor.execute(weekly_avg)
	weeklyavgload = cursor.fetchall()
	weeklyloadxaxis=[]
	weeklyloadcol1=[]
	weeklyloadcol2=[]
	for row in weeklyavgload:
		weeklyloadxaxis.append(row[4])
		weeklyloadcol2.append(round(float(row[3]),1))
		weeklyloadcol1.append(round(float(row[2]),1))
		
		
		
	#Page 2 storage_battery Query:
	cursor.execute(page2load)
	page22 = cursor.fetchall()
	xaxis=[]
	col1=[]
	col2=[]
	for row in page22:
		xaxis.append(str(row[0]))
		col1.append(float(row[1]))
		col2.append(float(row[2]))
	

					
	pagedata = {'xaxis':xaxis,'col1':col1,'col2':col2,'weeklyloadxaxis':weeklyloadxaxis,'weeklyloadcol2':weeklyloadcol2,'weeklyloadcol1':weeklyloadcol1}	
	return HttpResponse(json.dumps(pagedata),content_type="application/json")		
	
def getconsumptionpagedata(request):
	cursor = connection.cursor()
	#date = str(dt.date.today())
	#dateminus1 = str(dt.datetime.now() - dt.timedelta(days=1))
	date = '2016-01-27'
	dateminus1 = '2016-01-26'
	arg1 = request.GET.get('arg1')
	arg2 = request.GET.get('arg2')
	arg3 = request.GET.get('arg3')	
	arg4 = request.GET.get('arg4')
	arg5 = request.GET.get('arg5')
	arg6 = request.GET.get('arg6')
	arg7 = request.GET.get('arg7')
	if not arg1:	#Yesterday's  Metric Cross-sectional Comparison
		arg1 = dateminus1
	if not arg2:	#Today's  Metric Cross-sectional Comparison
		arg2 = date
	if not arg3:
		arg3 = 'SOLARPOWER'	
	if not arg6:
		arg6 = 'SOLARVOLTAGE'
	if not arg7:
		arg7 = 'SOLARPOWER'
	if not arg4: 
		arg4 = dateminus1
	if not arg5:
		arg5 = 	date
	
	metric_crossectional1 = "select  * from NEW_FUNC('"+arg1+"','"+arg2+"','"+arg3+"')"			#New-Metric-Cross-Sectional-Comparison
	#metric_crossectional1 = "select  * from NEW_FUNC('2016-01-10','2016-01-20','SOLARVOLTAGE') "			
	scatter = "SELECT * FROM P6('"+arg6+"','"+arg7+"','"+arg4+"','"+arg5+"')"						#Scatter-Plot
	#scatter = "SELECT * FROM P6('SOLARVOLTAGE','SOLARPOWER','2016-01-19','2016-01-20')"
	
	#scatter
	cursor.execute(scatter)
	scatter1 = cursor.fetchall()
	c1=[]
	c2=[]
	i=0
	for row in scatter1:
		c1.append([i,float(row[0])])
		c2.append([i,float(row[1])])
		i=i+1
	
	
	cursor.execute(metric_crossectional1)
	metric = cursor.fetchall()
	col1=[]
	col2=[]
	col3=[]
	for row in metric:
		col1.append(str(row[0]))	
		col2.append(float(row[2] or 0))
		col3.append(float(row[1] or 0))
		
		
	
	pagedata = {'c1':c1,'c2':c2,'col1':col1,'col2':col2,'col3':col3}
	return HttpResponse(json.dumps(pagedata), content_type = 'application/json')
	
def getstoragepagedata(request):	
	cursor = connection.cursor()		
	arg1 = request.session.get('arg1')
	arg2 = request.session.get('arg2')
	arg3 = request.session.get('arg3')
	date_time = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	realtimevoltagecompensation = "select * from retrieve_VoltageCompensation_new('"+loc+"','"+date_time+"')"    #Area_Chart

	a1=retrieve_4Chart(loc,date_time,6)
	
	aa1=[]
	aa2=[]
	aa3=[]
	aa4=[]
	aa5=[]	
	aa6=[]
	#New1-Chart	
	for res in a1:
		d=collections.OrderedDict()	
		d1=collections.OrderedDict()
		d2=collections.OrderedDict()
		d3=collections.OrderedDict()
		d4=collections.OrderedDict()
		d5=collections.OrderedDict()		
		d['x'] = str(res[1].date())
		d['y'] = float(res[2])
		aa1.append(d)	
		d4['x'] = str(res[1].date())
		d4['y'] = float(round(res[3],1))
		aa5.append(d4)
		d1['x'] = str(res[1].date())
		d1['y'] = float(round(res[4],1))
		aa2.append(d1)
		d2['x'] = str(res[1].date())
		d2['y'] = float(round(res[5],0))
		aa3.append(d2)
		d3['x'] = str(res[1].date())
		d3['y'] = float(round(res[6],0))
		aa4.append(d3)	
		d5['x'] = str(res[1].date())
		d5['y'] = float(round(res[7],1))
		aa6.append(d5)
			
	cursor.execute(realtimevoltagecompensation)
	batteryvoltageandtrendoutput = cursor.fetchall()	
	bc=[]
	bv=[]
	bc1=[]
	bv1=[]
	bt1=[]
	cur_thres=[]
	temp_thres=[]
	xaxis=[]
	i=0
	for row in batteryvoltageandtrendoutput:
		bc.append([i,round(float(row[5]),1)])
		bv.append([i,round(float(row[4]),1)])
		i=i+1	
		
	for row in 	batteryvoltageandtrendoutput:
		bc1.append(round(float(row[5]),1))
		bv1.append(round(float(row[4]),1))
		bt1.append(round(float(row[6]),1))
		cur_thres.append(round(float(row[7]),1))
		temp_thres.append(round(float(row[8]),1))
		xaxis.append(str(row[1]))
			
				
	pagedata = {'current_batteryresult':bc,'voltage_batteryresult':bv,'hc':bc1,'hc1':bv1,'cur_thres':cur_thres,'temp_thres':temp_thres,
	'xaxis':xaxis,'temp':bt1,'new_chart11':aa1,'new_chart21':aa2,'new_chart22':aa3,'new_chart23':aa4,'new_chart33':aa5,'new_chart44':aa6}
	return HttpResponse(json.dumps(pagedata), content_type = 'application/json')	
	
def getcreatelogdata(request):
	
	lst = request.GET.getlist('arg1[]')
	if len(lst) == 0:
		a="empty"
	if len(lst) != 0:
		#createlog(lst);
		import psycopg2
		from datetime import datetime
		from random import randint
		
		
		def insertData(d1,d2):
			#inserted_rows = df.shape[0]
			conn = psycopg2.connect("dbname='Philips' user='postgres' host='localhost' password='password' port='5555'")
			print "Hi"
			cur = conn.cursor()
			try:
				query1="insert into site_visit_log " \
				   "(site_log_id,site_id,user_id,date_time,visit_type)" \
				   "values("+str(d1['site_log_id'])+","+str(d1['site_id'])+","+str(d1['user_id'])+",'"+d1['date_time']+"','"+d1['visit_type']+"')"
				cur.execute(query1)
				count =0;
				for item in d2:

					query="insert into site_visit_details (site_log_id,asset_id,asset_condition,action_taken,comments)" \
						  " values ("+str(item['site_log_id'])+","+str(item['asset_id'])+",'"+item['asset_condition']+"','"+item['action_taken']+"','"+item['comments']+"')"
					
					cur.execute(query)
					count=count+1
				conn.commit()

			finally:
				conn.close()
			return count
			#inserted_rows

		date = str(dt.date.today())
		date_time = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))			
		sitelogid = randint(100000, 999999)				#Site-Log-ID generated randomly
		
		cursor = connection.cursor()
		userid = "select user_id from user_cred where case when l_name is null then f_name='"+lst[2]+"' else f_name||' '||l_name='"+lst[2]+"' end"  #User-ID
		cursor.execute(userid)
		userrid = cursor.fetchall()
		for row in userrid:
			uid = int(row[0])
		siteid = "select site_id from site where site_name = '"+lst[0]+"'"			#Site-ID
		cursor.execute(siteid)
		siteeid = cursor.fetchall()
		for row in siteeid:
			a = int(row[0])
		
		data1={'site_log_id':sitelogid,'site_id':a,'user_id':uid,'date_time':date_time,'visit_type':lst[1]}
		data2=[{'site_log_id': sitelogid, 'asset_id': 1, 'asset_condition':lst[3],'action_taken':lst[9],'comments':lst[15]},
				{'site_log_id': sitelogid, 'asset_id': 2, 'asset_condition':lst[4],'action_taken':lst[10],'comments':lst[16]},
				{'site_log_id': sitelogid, 'asset_id': 3, 'asset_condition':lst[5],'action_taken':lst[11],'comments':lst[17]},
				{'site_log_id': sitelogid, 'asset_id': 4, 'asset_condition':lst[6],'action_taken':lst[12],'comments':lst[18]},
				{'site_log_id': sitelogid, 'asset_id': 5, 'asset_condition':lst[7],'action_taken':lst[13],'comments':lst[19]},
				{'site_log_id': sitelogid, 'asset_id': 6, 'asset_condition':lst[8],'action_taken':lst[14],'comments':lst[20]},
				# {'site_log_id': sitelogid, 'asset_id': 7, 'asset_condition':'ok','action_taken':'none','comments':'asdsfver'},
				]

		
		count1=insertData(data1,data2)
		print count1		

	return HttpResponse(json.dumps(lst), content_type = 'application/json')	
	
# def createlog(lst):
	# import psycopg2
	# from datetime import datetime
	
	# def insertData(d1,d2):
		# #inserted_rows = df.shape[0]
		# conn = psycopg2.connect("dbname='Philips' user='postgres' host='localhost' password='password' port='5555'")
		# print "Hi"
		# cur = conn.cursor()
		# try:
			# query1="insert into site_visit_log " \
			   # "(site_log_id,site_id,user_id,date_time,visit_type)" \
			   # "values("+str(d1['site_log_id'])+","+str(d1['site_id'])+","+str(d1['user_id'])+",'"+d1['date_time']+"','"+d1['visit_type']+"')"
			# cur.execute(query1)
			# count =0;
			# for item in d2:

				# query="insert into site_visit_details (site_log_id,asset_id,asset_condition,action_taken,comments)" \
					  # " values ("+str(item['site_log_id'])+","+str(item['asset_id'])+",'"+item['asset_condition']+"','"+item['action_taken']+"','"+item['comments']+"')"
				
				# cur.execute(query)
				# count=count+1
			# conn.commit()

		# finally:
			# conn.close()
		# return count
		# #inserted_rows

	# data1={'site_log_id':10000014,'site_id':10001,'user_id':100002,'date_time':'2016-02-07 12:34:56','visit_type':lst[0]}
	# data2=[{'site_log_id': 10000014, 'asset_id': 1, 'asset_condition':'cleaning required','action_taken':'cleaning done','comments':'aasdsfver'},
			# {'site_log_id': 10000014, 'asset_id': 2, 'asset_condition':'cleaning required','action_taken':'cleaning done','comments':'aasdsfver'},
			# {'site_log_id': 10000014, 'asset_id': 3, 'asset_condition':'ok','action_taken':'none','comments':"new"},
			# {'site_log_id': 10000014, 'asset_id': 4, 'asset_condition':'ok','action_taken':'none','comments':'asdsfver'},
			# {'site_log_id': 10000014, 'asset_id': 5, 'asset_condition':'ok','action_taken':'none','comments':'asdsfver'},
			# {'site_log_id': 10000014, 'asset_id': 6, 'asset_condition':'ok','action_taken':'none','comments':'asdsfver'},
			# {'site_log_id': 10000014, 'asset_id': 7, 'asset_condition':'ok','action_taken':'none','comments':'asdsfver'},
			# ]

	
	# count1=insertData(data1,data2)
	# print count1
def getsitevisitlogdata(request):
	cursor = connection.cursor()
	
	fetchlatest = "select * from fetch_latest_one()"
	
	#Fetch-Latest-Record:
	cursor.execute(fetchlatest)
	fetchhlatest = cursor.fetchall()
	col1=[]
	col2=[]
	col3=[]
	col4=[]
	col5=[]
	col6=[]
	col7=[]
	for row in fetchhlatest:
		col1.append(str(row[0]))
		col2.append(str(row[1]))
		col3.append(str(row[2]))
		col4.append(str(row[3]))
		col5.append(str(row[4]))
		col6.append(str(row[5]))
		col7.append(str(row[6]))
		
	pagedata = {'col1':col1,'col2':col2,'col3':col3,'col4':col4,'col5':col5,'col6':col6,'col7':col7}
	return HttpResponse(json.dumps(pagedata),  content_type = 'application/json')
		
	
def getreviewlogdata(request):
	cursor = connection.cursor()
	strtdate = request.GET.get('strtdate')
	eddate = request.GET.get('eddate')
	
	if not strtdate:
		strtdate = '2016-01-22'
	if not eddate:
		eddate = '2016-02-26'
	
	#reviewdatee = "select * from fetch_details('"+reviewdate+"')"     #Review Log on click of list on multiple logs
	reviewlog = "select * from fetch_log_details('"+strtdate+"','"+eddate+"') order by datetime1 desc"	
	


	
	
	#Review-Log:
	cursor.execute(reviewlog)
	reviewwlog = cursor.fetchall()
	col1=[]
	col2=[]
	col3=[]
	for row in reviewwlog:
		col1.append(str(row[2])) 	#date
		col2.append(str(row[0]))	#malpe	
		col3.append(str(row[1]))	#action
		
	#pagedata = {'col1':col1,'col2':col2,'col3':col3,'rev_datecol1':rev_datecol1,'rev_datecol2':rev_datecol2,'rev_datecol3':rev_datecol3,'rev_datecol4':rev_datecol4,'rev_datecol5':rev_datecol5,'rev_datecol6':rev_datecol6}
	pagedata = {'col1':col1,'col2':col2,'col3':col3}
	return HttpResponse(json.dumps(pagedata),  content_type = 'application/json')
	
def getreviewlogdeatilsdata(request):
	cursor = connection.cursor()
	reviewdate = request.GET.get('reviewdate')

	if not reviewdate:
		reviewdate = '2016-02-26 13:19:36'	
	
	reviewdatee = "select * from fetch_details('"+reviewdate+"')"     #Review Log on click of list on multiple logs
	
	#Review-on-Date:
	cursor.execute(reviewdatee)
	reviewdateee = cursor.fetchall()
	rev_datecol1=[]
	rev_datecol2=[]
	rev_datecol3=[]
	rev_datecol4=[]
	rev_datecol5=[]
	rev_datecol6=[]	
	rev_datecol7=[]	
	for row in reviewdateee:
		rev_datecol1.append(str(row[0]))
		rev_datecol2.append(str(row[1]))
		rev_datecol3.append(str(row[2]))
		rev_datecol4.append(str(row[3]))
		rev_datecol5.append(str(row[4]))
		rev_datecol6.append(str(row[5]))	
		rev_datecol7.append(str(row[6]))	
	
	
	pagedata = {'rev_datecol1':rev_datecol1,'rev_datecol2':rev_datecol2,'rev_datecol3':rev_datecol3,'rev_datecol4':rev_datecol4,'rev_datecol5':rev_datecol5,'rev_datecol6':rev_datecol6,'rev_datecol7':rev_datecol7}
	return HttpResponse(json.dumps(pagedata),  content_type = 'application/json')
	
