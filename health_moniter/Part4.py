#Battery status : 
#Calculate Surplus, then calculate how much energy will be required for the rest of the day and next day till 6
#function to calculate, current Battery Capacty + Average Energy that will be Generated

	#crnt_timestamp='2015-10-15 15:15:15'
	#location='Malpe'
	#capacity=battery_capacity(location,crnt_timestamp)
	#data=load_hourly_consumption(crnt_timestamp)
	##result : best_case_dataframe,best_Case_message worst_case_dataframw,worst_case_message,load_consumpion
	#result=final_battery_results(data,5,[1,1,1,1,1],capacity)
	#a=trend_analyzer('Malpe','2015-11-15',5,15)
#def trend_analyzer(location):
	#current_ts=pd.to_datetime(current_ts)
	#current_hour=current_ts.hour
	#crnt_date=current_ts.date()
	#sql="select * from retrieve_avg_hourly_Inverter_consumption('"+location+"','"+str(crnt_date)+"',"+str(no_of_days)+");"
	#inverter=sql_execute(sql)
	#sql="select * from retrieve_avg_hourly_Load_consumption('"+location+"','"+str(crnt_date)+"',"+str(no_of_days)+");"
	#load=sql_execute(sql)
#
	##Converter Inverter and Load Data to Pandas Dataframe
	#load=pd.DataFrame(load)
	#columns=['Location','Load','Hour','Load_Energy']
	#load.columns=columns
	#load['Hour']=load['Hour'].convert_objects(convert_numeric=True)
#
	#inverter=pd.DataFrame(inverter)
	#inverter.columns=['Location','Hour','Inverter']
#
	#load=load.pivot(index='Hour',columns='Load',values='Load_Energy')
	#load['Hour']=load.index
#
	#final=pd.merge(inverter,load,on='Hour',how='inner')
#

from health_moniter.Setup_Base_Python_Functions import *

def final_battery_results(df,length,load_status,capacity):

	def df_to_list(df):
		list_final=[]
		for i in range(0,len(df)):
			row=list(df.iloc[i,:])
			row=tuple(row)
			list_final.append(row)
		return(list_final)
	current_battery_capacity=capacity[0]
	future_generation=capacity[1]
	#For Best Case Scenario :  Battery REmaining Hours [With the addition of Energy that will be generated in future]
	best_result=battery_remaining_hours(df,length,load_status,current_battery_capacity)
	total_sum=best_result['count'].sum()
	total_count=len(best_result['count'])
	if (total_sum == total_count):   
									best_message ="You are covered till next sunrise"
	else:
									best_message= "You are covered for the next "+str(best_result['count'].sum()) +" hours"

	#For Best Case Scenario :  Battery REmaining Hours [Assuing no more energy will be generated]
	worst_result=battery_remaining_hours(df,length,[1,1,1,1,1],current_battery_capacity+future_generation)
	if worst_result['count'].sum() == len(worst_result['count']):      
									worst_message ="You are covered till next sunrise"
	else:
									worst_message= "You are covered for the next "+str(best_result['count'].sum()) +" hours"
	#plotting the consumption of Energy from each load
	#ignoring first 2 and last 3 columns
	load_consumption_charts=best_result.ix[:,2:-3].sum()
	import pandas as pd
	aa=pd.DataFrame(load_consumption_charts)
	aa=pd.DataFrame(load_consumption_charts)
	aa['Load']=aa.index
	aa=df_to_list(aa)
	a1=list(load_consumption_charts.index)
	a2=list(load_consumption_charts)
	a=[a1,a2]
	best_result=df_to_list(best_result)
	final=[best_result,best_message,worst_result,worst_message,aa]
	#final=[best_result]

	# best_result=df_to_list(best_result)
	# a=[aa,best_result]
	# return a
	return(aa)	
	
def final_battery_results11(df,length,load_status,capacity):

	def df_to_list(df):
		list_final=[]
		for i in range(0,len(df)):
			row=list(df.iloc[i,:])
			row=tuple(row)
			list_final.append(row)
		return(list_final)
	current_battery_capacity=capacity[0]
	future_generation=capacity[1]
	#For Best Case Scenario :  Battery REmaining Hours [With the addition of Energy that will be generated in future]
	best_result=battery_remaining_hours(df,length,load_status,current_battery_capacity)
	total_sum=best_result['count'].sum()
	total_count=len(best_result['count'])
	if (total_sum == total_count):   
									best_message ="You are covered till next sunrise"
	else:
									best_message= "You are covered for the next "+str(best_result['count'].sum()) +" hours"

	#For Best Case Scenario :  Battery REmaining Hours [Assuing no more energy will be generated]
	worst_result=battery_remaining_hours(df,length,[1,1,1,1,1],current_battery_capacity+future_generation)
	if worst_result['count'].sum() == len(worst_result['count']):      
									worst_message ="You are covered till next sunrise"
	else:
									worst_message= "You are covered for the next "+str(best_result['count'].sum()) +" hours"
	#plotting the consumption of Energy from each load
	#ignoring first 2 and last 3 columns
	load_consumption_charts=best_result.ix[:,2:-3].sum()

	best_result=df_to_list(best_result)
	worst_result=df_to_list(worst_result)
	final=[best_result,worst_result]	
	return(final)

def battery_capacity(location,crnt_timestamp):
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)
	import pandas as pd
	crnt_timestamp=pd.to_datetime(crnt_timestamp)
	crnt_date=crnt_timestamp.date()
	future_generation=sql_execute("select * from retrieve_RemainingEnergyGeneration('"+location+"','"+str(crnt_timestamp)+"');")
	future_generation=future_generation[0][1]
	current_battery_capacity=sql_execute("select * from retrieve_Energy_Surplus_real_time('"+location+"','"+str(crnt_date)+"');")
	current_battery_capacity=current_battery_capacity[0][5]
	Capacity=[current_battery_capacity,future_generation]
	return(Capacity)


def load_hourly_consumption(location,current_ts,no_of_days):
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)
	import pandas as pd
	current_ts=pd.to_datetime(current_ts)
	current_hour=current_ts.hour
	crnt_date=current_ts.date()
	sql="select * from retrieve_avg_hourly_Inverter_consumption('"+location+"','"+str(crnt_date)+"',"+str(no_of_days)+");"
	inverter=sql_execute(sql)
	sql="select * from retrieve_avg_hourly_Load_consumption('"+location+"','"+str(crnt_date)+"',"+str(no_of_days)+");"
	load=sql_execute(sql)

	#Converter Inverter and Load Data to Pandas Dataframe
	load=pd.DataFrame(load)
	columns=['Location','Load','Hour','Load_Energy']
	load.columns=columns
	load['Hour']=load['Hour'].convert_objects(convert_numeric=True)

	inverter=pd.DataFrame(inverter)
	inverter.columns=['Location','Hour','Inverter']

	load=load.pivot(index='Hour',columns='Load',values='Load_Energy')
	load['Hour']=load.index

	final=pd.merge(inverter,load,on='Hour',how='inner')

	print(current_hour)
	if current_hour <=6:	
		final=final[final['Hour']< current_hour]
	else:
		one=final[final['Hour']<=6 ]
		final=one.append(final[final['Hour']>=current_hour])

	return(final)

#eg : dataframe, len =5, load_status=[1,1,1,-1,1]
def battery_remaining_hours(df,length,load_status,battery_capacity):
	import numpy as np
	import pandas as pd
	order=np.array(range(0,length))
	load_status=np.array(load_status)
	order=order*load_status
	order=order[order>=0]
	df=df.ix[:,order]
	df['Total']=df.ix[:,2:].sum(axis=1)
	df['cumsum']=df['Total'].cumsum()
	df['count']=np.where(df['cumsum']<=battery_capacity,1,0)
	return(df)

#------------------------------------------------------------------------------------------------------------------------
def trend_analyzer(location,current_dt,no_of_days,avg_days):
	def df_to_list(df):
                list_final=[]
                for i in range(0,len(df)):
                                row=list(df.ix[i,:])
                                row=tuple(row)
                                list_final.append(row)
                return(list_final)
	import pandas as pd
	import datetime as dt
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)

	load_raw=sql_execute("select * from retrieve_avg_trendAnalyzer_Load('"+location+"','"+current_dt+"',"+str(no_of_days)+");")
	load_raw=pd.DataFrame(load_raw)
	load_raw.columns=['key','Location','Date','Load','Hour','Energy']
	count=len(load_raw['Load'].unique())
	load=load_raw.pivot(index='key',columns='Load',values='Energy')

	load['Load Total']=load.ix[:,-count:].sum(axis=1)
	load['key']=load.index

	inv=sql_execute("select * from retrieve_avg_trendAnalyzer_Inverter('"+location+"','"+current_dt+"',"+str(no_of_days)+");")
	inv=pd.DataFrame(inv)
	inv.columns=['key','Location','Date','Hour','Inv Energy']
	final=load.merge(inv,on='key',how='inner')
	final['Total']=final[['Load Total','Inv Energy']].sum(axis=1)
	final=final[['Location','Date','Hour','Total']]
	day_count=final['Date'].unique()
	result=final.pivot(index='Hour',columns='Date',values='Total')
	result['Hour']=result.index

	#For 15 day average
	avg_date=pd.to_datetime(current_dt)
	avg_date=avg_date + dt.timedelta(days=-15)
	avg_inv=sql_execute("select * from retrieve_avg_hourly_Inverter_consumption('"+location+"','"+str(avg_date.date())+"',"+str(avg_days)+");")
	avg_inv=pd.DataFrame(avg_inv)
	avg_inv.columns=['Location','Hour','avg Inv Energy']
	avg_load=sql_execute("select * from retrieve_avg_hourly_Load_consumption('"+location+"','"+str(avg_date.date())+"',"+str(avg_days)+");")
	avg_load=pd.DataFrame(avg_load)
	avg_load.columns=['Location','Load','Hour','avg Load Energy']
	count=len(avg_load['Load'].unique())
	avg_load=avg_load.pivot(index='Hour',columns='Load', values='avg Load Energy')
	avg_load['Total Load']=avg_load.ix[:,-count:].sum(axis=1)
	avg_load['Hour']=avg_load.index
	avg_final=avg_load.merge(avg_inv,how="inner",on="Hour")
	avg_final['Total Avg Energy']=avg_final[['Total Load','avg Inv Energy']].sum(axis=1)
	avg_final=avg_final[['Location','Hour','Total Avg Energy']]

	combined=[result,avg_final]
	sample=df_to_list(avg_final)
	return(sample)
	
	
# def trend_analyzer1(location,current_dt,no_of_days,avg_days):
	# def df_to_list(df):
                # list_final=[]
                # for i in range(0,len(df)):
                                # row=list(df.ix[i,:])
                                # row=tuple(row)
                                # list_final.append(row)
                # return(list_final)
	# import pandas as pd
	# import datetime as dt
	# def sql_execute(sql):
		# import psycopg2
		# conn_string = "host='localhost' dbname='Philips_temp' user='postgres' password='password' port ='5555'"
		# con = psycopg2.connect(conn_string)
		# cursor = con.cursor()
		# cursor.execute(sql)
		# results=cursor.fetchall()
		# cursor.close()
		# return(results)

	# load_raw=sql_execute("select * from retrieve_avg_trendAnalyzer_Load('"+location+"','"+current_dt+"',"+str(no_of_days)+");")
	# load_raw=pd.DataFrame(load_raw)
	# load_raw.columns=['key','Location','Date','Load','Hour','Energy']
	# count=len(load_raw['Load'].unique())
	# load=load_raw.pivot(index='key',columns='Load',values='Energy')

	# load['Load Total']=load.ix[:,-count:].sum(axis=1)
	# load['key']=load.index

	# inv=sql_execute("select * from retrieve_avg_trendAnalyzer_Inverter('"+location+"','"+current_dt+"',"+str(no_of_days)+");")
	# inv=pd.DataFrame(inv)
	# inv.columns=['key','Location','Date','Hour','Inv Energy']
	# final=load.merge(inv,on='key',how='inner')
	# final['Total']=final[['Load Total','Inv Energy']].sum(axis=1)
	# final=final[['Location','Date','Hour','Total']]
	# day_count=final['Date'].unique()
	# result=final.pivot(index='Hour',columns='Date',values='Total')
	# result['Hour']=result.index
	# result=result.fillna(0)

	# #For 15 day average
	# avg_date=pd.to_datetime(current_dt)
	# avg_date=avg_date + dt.timedelta(days=-15)
	# avg_inv=sql_execute("select * from retrieve_avg_hourly_Inverter_consumption('"+location+"','"+str(avg_date.date())+"',"+str(avg_days)+");")
	# avg_inv=pd.DataFrame(avg_inv)
	# avg_inv.columns=['Location','Hour','avg Inv Energy']
	# avg_load=sql_execute("select * from retrieve_avg_hourly_Load_consumption('"+location+"','"+str(avg_date.date())+"',"+str(avg_days)+");")
	# avg_load=pd.DataFrame(avg_load)
	# avg_load.columns=['Location','Load','Hour','avg Load Energy']
	# count=len(avg_load['Load'].unique())
	# avg_load=avg_load.pivot(index='Hour',columns='Load', values='avg Load Energy')
	# avg_load['Total Load']=avg_load.ix[:,-count:].sum(axis=1)
	# avg_load['Hour']=avg_load.index
	# avg_final=avg_load.merge(avg_inv,how="inner",on="Hour")
	# avg_final['Total Avg Energy']=avg_final[['Total Load','avg Inv Energy']].sum(axis=1)
	# avg_final=avg_final[['Location','Hour','Total Avg Energy']]

	# combined=[result,avg_final]
	# result_col=result.columns
	# avg_final_col=avg_final.columns
	# sample_result=df_to_list(result)
	# sample_avg_final=df_to_list(avg_final)
	# final_list=[sample_result,result_col,sample_avg_final,avg_final_col]
	# return(final_list)	
	
def trend_analyzer1(location,current_dt,no_of_days,avg_days):
 def df_to_list(df):
                list_final=[]
                for i in range(0,len(df)):
                                row=list(df.ix[i,:])
                                row=tuple(row)
                                list_final.append(row)
                return(list_final)
 import pandas as pd
 import datetime as dt
 def sql_execute(sql):
  import psycopg2
  conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
  con = psycopg2.connect(conn_string)
  cursor = con.cursor()
  cursor.execute(sql)
  results=cursor.fetchall()
  cursor.close()
  return(results)

 load_raw=sql_execute("select * from retrieve_avg_trendAnalyzer_Load('"+location+"','"+current_dt+"',"+str(no_of_days)+");")
 load_raw=pd.DataFrame(load_raw)
 load_raw.columns=['key','Location','Date','Load','Hour','Energy']
 count=len(load_raw['Load'].unique())
 load=load_raw.pivot(index='key',columns='Load',values='Energy')

 load['Load Total']=load.ix[:,-count:].sum(axis=1)
 load['key']=load.index

 inv=sql_execute("select * from retrieve_avg_trendAnalyzer_Inverter('"+location+"','"+current_dt+"',"+str(no_of_days)+");")
 inv=pd.DataFrame(inv)
 inv.columns=['key','Location','Date','Hour','Inv Energy']
 final=load.merge(inv,on='key',how='inner')
 final['Total']=final[['Load Total','Inv Energy']].sum(axis=1)
 final=final[['Location','Date','Hour','Total']]
 day_count=final['Date'].unique()
 result=final.pivot(index='Hour',columns='Date',values='Total')
 result['Hour']=result.index
 result=result.fillna(0)

 #For 15 day average
 avg_date=pd.to_datetime(current_dt)
 avg_date=avg_date + dt.timedelta(days=-15)
 avg_inv=sql_execute("select * from retrieve_avg_hourly_Inverter_consumption('"+location+"','"+str(avg_date.date())+"',"+str(avg_days)+");")
 avg_inv=pd.DataFrame(avg_inv)
 avg_inv.columns=['Location','Hour','avg Inv Energy']
 avg_load=sql_execute("select * from retrieve_avg_hourly_Load_consumption('"+location+"','"+str(avg_date.date())+"',"+str(avg_days)+");")
 avg_load=pd.DataFrame(avg_load)
 avg_load.columns=['Location','Load','Hour','avg Load Energy']
 count=len(avg_load['Load'].unique())
 avg_load=avg_load.pivot(index='Hour',columns='Load', values='avg Load Energy')
 avg_load['Total Load']=avg_load.ix[:,-count:].sum(axis=1)
 avg_load['Hour']=avg_load.index
 avg_final=avg_load.merge(avg_inv,how="inner",on="Hour")
 avg_final['Total Avg Energy']=avg_final[['Total Load','avg Inv Energy']].sum(axis=1)
 avg_final=avg_final[['Location','Hour','Total Avg Energy']]

 combined=[result,avg_final]
 result_col=result.columns
 avg_final_col=avg_final.columns
 sample_result=df_to_list(result)
 sample_avg_final=df_to_list(avg_final)
 final_list=[sample_result,result_col,sample_avg_final,avg_final_col]
 return(final_list) 
	
	


# def consumption_stats(location,crnt_timestamp):
	# import pandas as pd
	# import datetime as dt
	# def sql_execute(sql):
		# import psycopg2
		# conn_string = "host='localhost' dbname='Philips_temp' user='postgres' password='password' port ='5555'"
		# con = psycopg2.connect(conn_string)
		# cursor = con.cursor()
		# cursor.execute(sql)
		# results=cursor.fetchall()
		# cursor.close()
		# return(results)
	# current_dt=pd.to_datetime(crnt_timestamp).date()
	# no_of_days=1

	# load_raw=sql_execute("select * from retrieve_avg_trendAnalyzer_Load('"+location+"','"+str(current_dt)+"',"+str(no_of_days)+");")
	# load_raw=pd.DataFrame(load_raw)
	# load_raw.columns=['key','Location','Date','Load','Hour','Energy']
	# count=len(load_raw['Load'].unique())
	# Load_Energy=load_raw.pivot(index='key',columns='Load',values='Energy')
	# Load_Energy=Load_Energy.add_suffix('_Energy')
	# Load_Energy['key']=Load_Energy.index

	# inv=sql_execute("select * from retrieve_avg_trendAnalyzer_Inverter('"+location+"','"+str(current_dt)+"',"+str(no_of_days)+");")
	# inv=pd.DataFrame(inv)
	# inv.columns=['key','Location','Date','Hour','Inv Energy']
	# Energy=inv.merge(Load_Energy,on='key',how='inner')
	# Energy['Date']=pd.to_datetime(Energy['Date'])
	# Energy=Energy[Energy['Date']==pd.to_datetime(current_dt)]
	# Energy['Hour']=                Energy['Hour'].astype(float)
	# Energy['Date']=pd.to_datetime(Energy['Date'])
	# inv_power=sql_execute("select * from retrieve_ConsumptionStats_Inverter('"+location+"','"+crnt_timestamp+"');");
	# inv_power=pd.DataFrame(inv_power)
	# inv_power.columns=['Location','Date','Hour','inv_power']

	# load_power=sql_execute("select * from retrieve_ConsumptionStats_Load('"+location+"','"+crnt_timestamp+"');");
	# load_power=pd.DataFrame(load_power)
	# load_power.columns=['Location','Date','Load','Hour','Load_Voltage','Load_power']


	# count=len(load_power['Load'].unique())
	# load_power=load_power.pivot(index='Hour',columns='Load',values='Load_power')
	# load_power=load_power.add_suffix('_Power')
	# load_power['Hour']=load_power.index

	# Power=load_power.merge(inv_power,how="inner",on='Hour')
	# Power['Hour']=Power['Hour'].astype(float)
	# Power['Date']=pd.to_datetime(Power['Date'])
	# Final=Power.merge(Energy,how='inner',on=['Hour','Location','Date'])  
	# col=Final.columns

	# def df_to_list(df):
					# list_final=[]
					# for i in range(0,len(df)):
									# row=list(df.ix[i,:])
									# row=tuple(row)
									# list_final.append(row)
					# return(list_final)
	# Final=df_to_list(Final)
	# a=[Final,col]

	# return(a)
import pandas as pd	
import datetime as dt
import psycopg2

def consumption_stats(location,crnt_timestamp):
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)

	current_dt=pd.to_datetime(crnt_timestamp).date()
	no_of_days=1

	load_raw=sql_execute("select * from retrieve_Energy_ConsumptionStats_Load('"+location+"','"+str(current_dt)+"',"+str(no_of_days)+");")
	load_raw=pd.DataFrame(load_raw)
	load_raw.columns=['key','Location','Date','Load','Hour','Energy']
	count=len(load_raw['Load'].unique())
	Load_Energy=load_raw.pivot(index='key',columns='Load',values='Energy')
	Load_Energy=Load_Energy.add_suffix('_Energy')
	Load_Energy['key']=Load_Energy.index

	inv=sql_execute("select * from retrieve_Energy_ConsumptionStats_Inverter('"+location+"','"+str(current_dt)+"',"+str(no_of_days)+");")
	inv=pd.DataFrame(inv)
	inv.columns=['key','Location','Date','Hour','Inv Energy']
	Energy=inv.merge(Load_Energy,on='key',how='inner')
	Energy['Date']=pd.to_datetime(Energy['Date'])
	Energy=Energy[Energy['Date']==pd.to_datetime(current_dt)]
	Energy['Hour']= Energy['Hour'].astype(float)
	Energy['Date']=pd.to_datetime(Energy['Date'])
	inv_power=sql_execute("select * from retrieve_ConsumptionStats_Inverter('"+location+"','"+crnt_timestamp+"');");
	inv_power=pd.DataFrame(inv_power)
	inv_power.columns=['Location','Date','Hour','inv_power']

	load_power=sql_execute("select * from retrieve_ConsumptionStats_Load('"+location+"','"+crnt_timestamp+"');");
	load_power=pd.DataFrame(load_power)
	load_power.columns=['Location','Date','Load','Hour','Load_Voltage','Load_Power']


	count=len(load_power['Load'].unique())
	load_power=load_power.pivot(index='Hour',columns='Load',values='Load_Power')
	load_power=load_power.add_suffix('_Power')
	load_power['Hour']=load_power.index

	Power=load_power.merge(inv_power,how="inner",on='Hour')
	Power['Hour']=Power['Hour'].astype(float)
	Power['Date']=pd.to_datetime(Power['Date'])
	Final=Power.merge(Energy,how='inner',on=['Hour','Location','Date']) 
	col=Final.columns

	def df_to_list(df):
		list_final=[]
		for i in range(0,len(df)):
			row=list(df.ix[i,:])
			row=tuple(row)
			list_final.append(row)
		return(list_final)
	Final=df_to_list(Final)
	a=[Final,col]

	return(a)
	
def consumption_stats11(location,crnt_timestamp):
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)
	current_dt=pd.to_datetime(crnt_timestamp).date()
	no_of_days=1

	load_raw=sql_execute("select * from retrieve_Energy_ConsumptionStats_Load('"+location+"','"+str(current_dt)+"',"+str(no_of_days)+");")
	load_raw=pd.DataFrame(load_raw)
	load_raw.columns=['key','Location','Date','Load','Hour','Energy']
	count=len(load_raw['Load'].unique())
	Load_Energy=load_raw.pivot(index='key',columns='Load',values='Energy')
	Load_Energy=Load_Energy.add_suffix('_Energy')
	Load_Energy['key']=Load_Energy.index
	

	inv=sql_execute("select * from retrieve_Energy_ConsumptionStats_Inverter('"+location+"','"+str(current_dt)+"',"+str(no_of_days)+");")
	inv=pd.DataFrame(inv)
	inv.columns=['key','Location','Date','Hour','Inv Energy']
	inv.sort('Inv Energy')
	Energy=inv.merge(Load_Energy,on='key',how='inner')
	Energy['Date']=pd.to_datetime(Energy['Date'])
	Energy=Energy[Energy['Date']==pd.to_datetime(current_dt)]
	Energy['Hour']=	Energy['Hour'].astype(float)
	Energy['Date']=pd.to_datetime(Energy['Date'])
	Energy=Energy.sort('Hour')
	for i in Energy.columns[4:]:
		Energy[i]=Energy[i].cumsum()

	inv_power=sql_execute("select * from retrieve_ConsumptionStats_Inverter('"+location+"','"+crnt_timestamp+"');");
	inv_power=pd.DataFrame(inv_power)
	inv_power.columns=['Location','Date','Hour','inv_power']

	load_power=sql_execute("select * from retrieve_ConsumptionStats_Load('"+location+"','"+crnt_timestamp+"');");
	load_power=pd.DataFrame(load_power)
	load_power.columns=['Location','Date','Load','Hour','Load_Voltage','Load_Power']


	count=len(load_power['Load'].unique())
	load_power=load_power.pivot(index='Hour',columns='Load',values='Load_Power')
	load_power=load_power.add_suffix('_Power')
	load_power['Hour']=load_power.index

	Power=inv_power.merge(load_power,how="inner",on='Hour')
	Power['Hour']=Power['Hour'].astype(float)
	Power['Date']=pd.to_datetime(Power['Date'])
	Final=Power.merge(Energy,how='inner',on=['Hour','Location','Date'])	
	base=list(['key','Hour','Location','Date'])
	energy_col=[x for x in Final.columns if x.endswith('ergy')]
	
	power_col=[x for x in Final.columns if x.endswith('ower')]
	Final=Final[base+power_col+energy_col]
	Final=Final.sort('Hour')
	col=Final.columns
	def df_to_list(df):
		list_final=[]
		for i in range(0,len(df)):
			row=list(df.ix[i,:])
			row=tuple(row)
			list_final.append(row)
		return(list_final)
	Final=df_to_list(Final)
	Final=sorted(Final,key=lambda tup:tup[1])
	a=[Final,col]

	return(a)
	
	
def retrieve_3Chart(location,crnt_timestamp,no_of_days):

	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)

	def df_to_list(df):

					list_final=[]

					for i in range(0,len(df)):

									row=list(df.ix[i,:])

									row=tuple(row)

									list_final.append(row)

					return(list_final)

	#Get the Inverter 

	sql="select * from retrieve_3Chart_final('"+location+"','"+crnt_timestamp+"',"+str(no_of_days)+");"

	table=sql_execute(sql)

	table=pd.DataFrame(table)

	columns=['Location','Date','Energy_Generated','Battery_Energy_Remaining','Inverter_Energy_Consumed']

	table.columns=columns

	table['Date']=pd.to_datetime(table['Date'])

	timestamp=pd.to_datetime(crnt_timestamp)

	if (timestamp.hour <= 18):

					end_date=timestamp - dt.timedelta(days=1)

	else:

					end_date=timestamp

	start_date=end_date - dt.timedelta(days=no_of_days)

	sql="select distinct location, date, load,(total_loadenergy - first_loadenergy) \
	as Load_Energy from Load_dailystats where date between '"+\
	str(start_date.date())+"' and '"+str(end_date.date())+"' and location = '"+location+"';"

	a=sql_execute(sql)

	a=pd.DataFrame(a)

	a.columns=['Location','Date','Load','Load_Energy']

	a['Date']=pd.to_datetime(a['Date'])

	b=a.pivot(index='Date',columns='Load',values='Load_Energy')

	b['Date']=b.index

	c=table.merge(b,how='inner',on='Date')
	c['Date']=(c['Date']).map(lambda x:str(x.strftime("%Y-%m-%d")))



	#Convert Table to list of tuples

	final=df_to_list(c)

	return(final)
	
	
def retrieve_4Chart(location,crnt_timestamp,no_of_days):
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)
	import	pandas as pd
	def df_to_list(df):
		list_final=[]
		for i in range(0,len(df)):
			row=list(df.ix[i,:])
			row=tuple(row)
			list_final.append(row)
		return(list_final)
	#Get the Inverter Energy Consumed, Battery Energy Remaining and Energy Generated
	sql="select * from retrieve_3Chart_final('"+location+"','"+crnt_timestamp+"',"+str(no_of_days)+");"
	table=sql_execute(sql)
	table=pd.DataFrame(table)
	columns=['Location','Date','Energy_Generated','Battery_Energy_Remaining','Inverter_Energy_Consumed']
	table.columns=columns
	table['Date']=pd.to_datetime(table['Date'])
	timestamp=pd.to_datetime(crnt_timestamp)
	if (timestamp.hour <= 18):
		end_date=timestamp - dt.timedelta(days=1)
	else:
		end_date=timestamp
	#Get the load level Energy Consumed Data
	start_date=end_date - dt.timedelta(days=no_of_days)
	sql="select distinct location, date, load,avg(total_loadenergy - first_loadenergy) \
	as Load_Energy from Load_dailystats where date between '"+\
	str(start_date.date())+"' and '"+str(end_date.date())+"' and location = '"+location+"' \
	group by location,date,load;"
	a=sql_execute(sql)
	a=pd.DataFrame(a)
	a.columns=['Location','Date','Load','Load_Energy']
	a['Date']=pd.to_datetime(a['Date'])
	b=a.pivot(index='Date',columns='Load',values='Load_Energy')
	b['Date']=b.index
	c=table.merge(b,how='inner',on='Date')

	#Get the Length of Day Data
	sql="select * from day_length('"+location+"','"+crnt_timestamp+"',"+str(no_of_days)+");"
	day=sql_execute(sql)
	day=pd.DataFrame(day)
	day.columns=['Date','day_length']
	day['Date']=pd.to_datetime(day['Date'])

	d=c.merge(day,on='Date',how='inner')
	#Convert Table to list of tuples
	final=df_to_list(d)
	return(final)	
	
def consumption_trend(location,current_dt,no_of_days,avg_days):
	import numpy as np
	def df_to_list(df):
	            list_final=[]
	            for i in range(0,len(df)):
	                            row=list(df.ix[i,:])
	                            row=tuple(row)
	                            list_final.append(row)
	            return(list_final)
	#Execute the Trend Analyzer1 Function to get the past 7 days and 15 day avg energy Consumption Data
	result=trend_analyzer1(location,current_dt,no_of_days,avg_days)

	#Extract the past 7 days hourly consumption data
	past_7_day_data=result[0]
	a=pd.DataFrame(past_7_day_data)
	last_col=a.columns[-1]
	a=a.sort(last_col)
	#Except for the last column, take a cumsum for all columns
	for i in a.columns[:-1]:
		a[i]=a[i].cumsum()
	current_day_col=a.columns[-2]
	a[current_day_col] = np.where(a[current_day_col]==a[current_day_col].shift(1), 0,a[current_day_col])	
	a=df_to_list(a)

	avg_15_day_data=result[2]
	b=pd.DataFrame(avg_15_day_data)
	last_col=b.columns[1]
	b=b.sort(last_col)
	b.iloc[:,-1]=b.iloc[:,-1].cumsum()
	b=df_to_list(b)
	return(a,result[1],b,result[3])	

	
def custom_battery_results(df,length,load_status,capacity,total):
	import decimal
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)
	def load_hourly_consumption(location,current_ts,no_of_days):
		current_ts=pd.to_datetime(current_ts)
		current_hour=current_ts.hour
		crnt_date=current_ts.date()
		sql="select * from retrieve_avg_hourly_Inverter_consumption('"+location+"','"+str(crnt_date)+"',"+str(no_of_days)+");"
		inverter=sql_execute(sql)
		sql="select * from retrieve_avg_hourly_Load_consumption('"+location+"','"+str(crnt_date)+"',"+str(no_of_days)+");"
		load=sql_execute(sql)

		#Converter Inverter and Load Data to Pandas Dataframe
		load=pd.DataFrame(load)
		columns=['Location','Load','Hour','Load_Energy']
		load.columns=columns
		load['Hour']=load['Hour'].convert_objects(convert_numeric=True)

		inverter=pd.DataFrame(inverter)
		inverter.columns=['Location','Hour','Inverter']

		load=load.pivot(index='Hour',columns='Load',values='Load_Energy')
		load['Hour']=load.index

		final=pd.merge(inverter,load,on='Hour',how='inner')

		print(current_hour)
		if current_hour <=6:	
			final=final[final['Hour']< current_hour]
		else:
			one=final[final['Hour']<=6 ]
			final=one.append(final[final['Hour']>=current_hour])

		return(final)
	import numpy as np	
	crnt_timestamp = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	data=load_hourly_consumption('Malpe',crnt_timestamp,5)
	#result : best_case_dataframe,best_Case_message worst_case_dataframw,worst_case_message,load_consumpion
	battery_capacity=capacity[0]
	count=decimal.Decimal(str(data.count()[0]))
	val=total/count
	data['Total']=val
	data['cumsum']=data['Total'].cumsum()
	data['count']=np.where(data['cumsum']<=battery_capacity,1,0)

	total_sum=data['count'].sum()
	total_count=len(data['count'])
	if (total_sum == total_count):	
			best_message ="You are covered till next sunrise"
	else:
			best_message= "You are covered for the next "+str(data['count'].sum()) +" hours"
	return(best_message)	
	
def energry_forecast(location,current_date,no_of_days):
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)
	from pandas.stats.moments import ewma
	import numpy as np

	def predict(x,span,periods = 5):     
	    x_predict = np.zeros((span+periods,))
	    x_predict[:span] = x[-span:]
	    pred =  ewma(x_predict,span)[span:]
	    return pred

	def sql_execute(sql):
	    import psycopg2
	    conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
	    con = psycopg2.connect(conn_string)
	    cursor = con.cursor()
	    cursor.execute(sql)
	    results=cursor.fetchall()
	    cursor.close()
	    return(results)

	def df_to_list(df):
		list_final=[]
		for i in range(0,len(df)):
			row=list(df.iloc[i,:])
			row=tuple(row)
			list_final.append(row)
		return(list_final)
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)	
	current_dt=current_date=pd.to_datetime(current_date)
	current_date=current_date -dt.timedelta(days=1)
	start_date= current_date - dt.timedelta(days=30)
	sql="select location,date, (total_solarenergy - first_solarenergy) from solarpanel_dailystats \
		 where date between '"+str(start_date)+"' \
		 and '"+str(current_date)+"' and location='"+location+"';"
	data=pd.DataFrame(sql_execute(sql))
	data.columns=['location','date','Solar Energy']
	data.index=pd.to_datetime(data['date'])
	data=data.sort('date')
	predicted=predict(data['Solar Energy'],14,no_of_days)
	predicted=pd.DataFrame(predicted,index=None)
	predicted.columns=['predicted_value']
	future_dates=pd.date_range(current_dt,current_date+dt.timedelta(days=no_of_days))
	predicted['predicted_on']=current_dt
	predicted['location']=location
	predicted['predicted_for']=future_dates
	predicted=predicted[['location','predicted_on','predicted_for','predicted_value']]
	last_update=sql_execute("select max(cast(predicted_on as date)) from energy_predictions")
	if pd.to_datetime(last_update[0][0])<pd.to_datetime(current_dt):
		sql_insert(predicted,'energy_predictions')	
	else:
		print("no insert")
	def sql_execute(sql):
		import psycopg2
		conn_string = "host='localhost' dbname='Philips' user='postgres' password='password' port ='5555'"
		con = psycopg2.connect(conn_string)
		cursor = con.cursor()
		cursor.execute(sql)
		results=cursor.fetchall()
		cursor.close()
		return(results)
		
		
	sql="select predicted_for,predicted_energy \
	from energy_predictions \
	 where cast(predicted_on as date) between cast('"+str(current_date -dt.timedelta(days=5))+"' as date)\
	 	and cast('"+str(current_date)+"' as date)   \
	 and cast(predicted_on as date)+1=cast(predicted_for as date) \
	 order by predicted_for"
	an=sql_execute(sql)
	an=pd.DataFrame(an)
	an.columns=['date','value']
	bb=an.merge(data,on='date')
	bb['Error']=abs(bb['Solar Energy'] - bb['value'])
	bb['predicted']=0
	bb=bb[['date','value','predicted','Error']]
	#current_generation=sql_execute("select energy_generated1 from retrieve_Energy_Generated_real_time('"+location+"','"+str(current_dt)+"');")
	predicted['value']=0
	predicted=predicted[['predicted_for','value','predicted_value']]
	predicted.columns=['date','value','predicted']
	predicted['Error']=0
	c=bb.append(predicted)
	c['date']=pd.to_datetime(c['date'])
	columns=c.columns
	c=df_to_list(c)
	return([c,columns])	