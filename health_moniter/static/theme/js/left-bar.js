/* Configuration For Left Menu */


var left_menu = [
	{ 
		key:'Overview', 
		link:'overview',
		img:'fa fa-home',
		menu:[]
	},
	/* { 
		key:'Log Overview', 
		link:'log_overview',
		img:'fa fa-file-text-o',
		menu:[]
	}, */
	
	{ 
		key:'EDA', 
		link:'generation',
		img:'fa fa-search',
		menu:[]
	},
	{ 
		key:'Advanced EDA', 
		link:'consumption',
		img:'fa fa-search-plus',
		menu:[]
	},
	{ 
		key:'Site Visit Log', 
		link:'Site_Visit_Log',
		img:'fa fa-street-view',
		menu:[]
	}	
];