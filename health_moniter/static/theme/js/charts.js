
function new_stacked_bar_chart(chartid, xvalue, yvalue, data) {
	console.log(data);
	nv.addGraph(function () {
		var chart = nv.models.multiBarChart().stacked(true)
			.showControls(false) 
			;
			
			
			tooltip = function(key, y, e, graph) {
				return '<h3>' + key + '</h3>' + '<p>' +  W + '</p>'
			}

		chart.xAxis     	
				.axisLabel(xvalue)
					
		chart.yAxis     
				.axisLabel(yvalue)
				.tickFormat(d3.format(',r'));
				chart.color(["#C7A610",'rgba(51, 122, 164, 0.95)','green'])
				
		d3.select(chartid)
				.append('svg')
				.datum(data)
				.call(chart);
		nv.utils.windowResize(chart.update);
		return chart;
	});
}

//-----------------------------------------------NEW_BARCHART---(1st chart)-------------------------------------------------------------------
function new_barchart(chartid, xvalue, yvalue, data) {
	
	nv.addGraph(function () {
    var chart = nv.models.discreteBarChart()
		.margin({top: 30, right: 10, bottom: 50, left: 60}) 	
      .rotateLabels(0)      //Angle to rotate x-axis labels.
	  .showValues(true) 	 
	  .color(['#C7A610']);
    
		chart.xAxis     	
				.axisLabel(xvalue)
					
		chart.yAxis     
				.axisLabel(yvalue)
				
				.tickFormat(d3.format(',r'));
						
		d3.select(chartid)
				.append('svg')
				.datum(data)
				.call(chart);
		nv.utils.windowResize(chart.update);
		return chart;
	});
}
//-----------------------------------------------LINECHART2----(2nd chart)-------------------------------------------------------------------------
function linechart2(chartid, xvalue, yvalue, data) {
	
	nv.addGraph(function () {
		var chart = nv.models.lineChart()
				.margin({left: 65}) 
				.useInteractiveGuideline(true)  
				.showLegend(true)   
				.showYAxis(true)        
				.showXAxis(true) 
				a=-1
				for(i=0;i<data[0].values.length;i++){
							a=a+1
							//console.log(a);
						var x = function(a){
							 if(a === 0)
								return "12 AM"
							else if(a === 1)
								return "1 AM"
							else if(a === 2)
								return "2 AM"					
							else if(a === 4)
								return "4 AM"
							else if(a ===3)
								return "3 AM"
							else if(a === 5)
								return "5 AM" 
							else if(a === 6)
								return "6 AM" 							
							else if(a === 7)
								return "7 AM"
							else if(a === 8)
								return " 8 AM"
							else if(a === 9)
								return "9 AM"
							else if(a === 10)
								return "10 AM"
							else if(a === 11)
								return "11 AM"
							else if(a === 12)
								return "12 PM"
							else if(a === 13)
								return "1 PM"
							else if(a === 14)
								return "2 PM"
							else if(a === 15)
								return "3 PM"
							else if(a === 16)
								return "4 PM"
							else if(a === 17)
								return "5 PM"
							else if(a === 18)
								return "6 PM"
							else if(a === 19)
								return "7 PM"
							else if(a === 20)
								return "8 PM"
							else if(a === 21)
								return "9 PM"
							else if(a === 22)
								return "10 PM"
							else if(a === 23)
								return "11 PM"							
						 }; 
						
					 chart.xAxis     
					  .axisLabel('Time ( Hours )')
					  .tickFormat(x); 
				}	  
				
		chart.yAxis     //Chart y-axis settings
				.axisLabel(yvalue)
				 .tickFormat(d3.format(',w'));
		d3.select(chartid).selectAll("svg").remove();
		d3.select(chartid)    
				.append('svg')
				.datum(data)         
				.call(chart); 

		nv.utils.windowResize(function () {
			chart.update()
		});
		return chart;
	});
}


 function linechartandbarchart(chartid, xvalue, yvalue, data) {
	console.log(xvalue)
  nv.addGraph(function() {
      var chart = nv.models.linePlusBarChart()
			 
			.margin({top: 30, right: 60, bottom: 50, left: 100})
			.x(function(d,i) { return d[0] })
            .y(function(d) { return d[1] })
			.options({focusEnable: false })// if u need, u can enable focus chart	
			;
		var days = ["6 AM", "", "8 AM", "", "10 AM", "", "12 PM","","2 PM","","4 PM","","6 PM","","8 PM","","10 PM","","12 AM","","2 AM","3 AM","4 AM","5 AM"];	
		 
		 chart.xAxis
			.tickValues([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23])
			.axisLabel(xvalue)
			.staggerLabels(true)
			.tickFormat(function(d){
			  return days[d]
			});
		chart.y1Axis
			.axisLabel(yvalue)
          .tickFormat(d3.format(',.f'));
		  
		chart.y2Axis
			.axisLabel(yvalue)
          .tickFormat(function(d) {
			  return d3.format(',.f')(d) + 'v' 
			});
			
			/*chart.tooltip.contentGenerator(function(key) {
				console.log(key);
				//return '<p>'+data['series'][0].color+','+data['series'][0].value+'</p>'
				//return data['point'];
			});
			/*			
			chart.tooltipContent(function (key, x, y, e, graph) {
				return '<p><strong>' + key + '</strong></p>' +
					   '<p>' + e.value + ' in the month ' + x + '</p>';
				});*/
			
		
		 chart.bars.forceY([0]);
			
      d3.select(chartid)
		.append('svg')
        .datum(data)
        .transition()
        .duration(0)
        .call(chart);
      nv.utils.windowResize(chart.update);
      return chart;
  });
  

} 

function stackedareachart(chartid, xvalue, yvalue, data) {
  nv.addGraph(function() {
	var width = 1000, height = 800;  
    var chart = nv.models.lineChart()
                  .margin({right: 100})
				  //.interpolate("bundle")      //basis,bundle,linear,step-before,step-after,basis-open,cardinal,monotone   
                  .x(function(d,i) { return i })   
                  .y(function(d,i) {return d[1] })
	var days = ["12 AM","1 AM","2 AM","3 AM","4 AM","5 AM","6 AM","7 AM","8 AM","9 AM","10 AM","11 AM","12 PM","1 PM","2 PM","3 PM","4 PM","","6 PM","", "8 PM","","10 PM","11 PM"];

		 chart.xAxis
			.tickValues([0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23])
			.axisLabel(xvalue)
			.tickFormat(function(d){
			  return days[d]
			});
		chart.yAxis     
				.axisLabel(yvalue);		
		d3.select(chartid).selectAll("svg").remove();
		d3.select(chartid)     
				.append('svg')
				.datum(data)         
				.call(chart);
    nv.utils.windowResize(chart.update);
    return chart;
  });
}

function consumption_stats_area_chart(chartid, xvalue, yvalue, data) {
  nv.addGraph(function() {
	//var width = 1000, height = 800;  
    var chart = nv.models.stackedAreaChart()
                  .margin({right: 100})
                  .x(function(d) { return d[0] })   //We can modify the data accessor functions...
                  .y(function(d) { return d[1] })   //...in case your data is formatted differently.
                  .useInteractiveGuideline(true)    //Tooltips which show all data points. Very nice!
                  .rightAlignYAxis(false)      //Let's move the y-axis to the right side.
                  //.transitionDuration(500)
                  .showControls(false)       //Allow user to choose 'Stacked', 'Stream', 'Expanded' mode.
                  .clipEdge(true);
	var days = ["12 AM","1 AM","2 AM","3 AM","4 AM","5 AM","6 AM","7 AM","8 AM","9 AM","10 AM","11 AM","12 PM","1 PM","2 PM","3 PM","4 PM","5 PM","6 PM","7 PM","8 PM","9 PM","10 PM","11 PM"];

		 chart.xAxis
			.tickValues([0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23])
			.axisLabel(xvalue)
			.tickFormat(function(d){
			  return days[d]
			});
		chart.yAxis   
				.axisLabel(yvalue)
				.tickFormat(d3.format(',.2f'));		
		d3.select(chartid).selectAll("svg").remove();
		d3.select(chartid)     
				.append('svg')
				.datum(data)         
				.call(chart);
    nv.utils.windowResize(chart.update);
    return chart;
  });
}


function highchart(chartid, xvalue, yvalue, data){
	/* s = data[2].data
	for(i=0;i<s.length;i++){
		s = Math.min(s[i])
		console.log(s)
	} */
	//console.log(s)
$(function () {
     Highcharts.setOptions({
    // colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4']
	 colors: [ "#C7A610",'rgba(51, 122, 164, 0.95)','green']
    });	
    $(chartid).highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Voltage Compensation versus Time'
        },	
        credits: {
            enabled: false
        },		
    xAxis: {
		categories: data[0].data,
        labels: {
            style: {
                color: '#000',
                font: '9px Trebuchet MS, Verdana, sans-serif'
            }
        },
        
        tickPixelInterval: 80,
        tickmarkPlacement: 'on'
    },
        yAxis: {
            title: {
                text: 'Voltage'
            },
            labels: {
				format: '{value:f}' +'v',

               
            },
			min:0,
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.y:,.f}</b><br/>'
        },

        series: [{
            name: data[0].name,
            data: data[0].data
        }, {
            name: data[1].name,
            data: data[1].data
        },
]
    });
});		
}
function highchart1(chartid, xvalue, yvalue, data){
	console.log(data);
	s = data[3].data
	for(i=0;i<s.length;i++){
		s = Math.min(s[i])
		console.log(s)
	}	
	console.log(s)
$(function () {
	 colors: [ "#C7A610",'rgba(51, 122, 164, 0.95)','green']
    $(chartid).highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },		
    xAxis: {
		categories: data[0].data,
        labels: {
            style: {
                color: '#000',
                font: '9px Trebuchet MS, Verdana, sans-serif'
            }
        },
        
        tickPixelInterval: 80,
        tickmarkPlacement: 'on'
    },
        yAxis: {
            title: {
                text: 'Voltage'
            },
            labels: {
                formatter: function () {
                    return this.value  + 'v';
                }
            },
			min:s,
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.y:,.f}</b><br/>'
        },

        series: [{
            name: data[1].name,
            data: data[1].data
        }, {
            name: data[2].name,
            data: data[2].data
        }, {
			name: data[3].name,
			data: data[3].data
		}
]
    });
});		
}	
/*------------------------------scatter----------------------------------*/
function scatterr(chartid, xvalue, yvalue, data){
	//console.log(data[0].data)
$(function () {
    $(chartid).highcharts({
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
		credits: {
            enabled: false
        },		
        subtitle: {
            text: ''
        },
        xAxis: {
			min:0,
            title: {
                enabled: true,
                text: 'Values'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
        },
        yAxis: {
			tickInterval: 500,
            title: {
                text: 'Values'
            },
            labels: {
                formatter: function () {
                    return this.value  + '';
                }
            },			
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#fff',
            borderWidth: 1
        },
        plotOptions: {
			
            scatter: {
                marker: {
                    radius: 4,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>'
                    /* pointFormat: '{point.x} cm, {point.y} kg' */
                }
            }
        },
        series: [{
            name: xvalue,
            color: 'rgba(223, 83, 83, .5)',
            data: data[0].data

        }, {
            name: yvalue,
            color: 'rgba(119, 152, 191, .5)',
            data: data[1].data
        }]
    });
});
}
//-----------------------------------------------------Highchart Line Chart -----------------------------------------

function highline(id,xaxis,yaxis,data){
	//console.log(data);
$(function () {
    $(id).highcharts({
        title: {
            text: '',
            x: -20 //center
        },
		credits:{enabled:false},
        subtitle: {text: '',x: -20},
        xAxis: { categories: data[0].data,labels: {step: 2},labels:{style: {color: 'black'}}},
		
        yAxis: {tickInterval: 100,title: {text: yaxis},plotLines: [{value: 0,width: 1,color: 'red'}],labels: {style: {color: 'red'}}},
		
        tooltip: {valueSuffix: ''},
		
     /*    legend: {layout: 'vertical',align: 'right',verticalAlign: 'top',x:-10,y:10,floating: true, borderWidth: 0,backgroundColor:'#5DA7C5',borderColor:'#007095', borderRadius: 2,borderWidth:1,color:'white', symbolHeight: 20,symbolWidth: 12, symbolRadius: 6,
		        itemHoverStyle: {
                color: '#FFF'
            }}, */
			
		legend:{backgroundColor: '#FFFFFF', floating: true, layout: 'vertical', verticalAlign: 'top',  x: 90, y: 60, shadow: true,align: 'right',verticalAlign: 'top',x:-10,y:10,
		    itemHoverStyle: {
                color: 'orange'
            }},	
         plotOptions: {
            series: {
                animation: {
                    duration: 2000
                }
            }
        }, 
        series: [{
            name: data[1].key,
            data: data[1].data,
			'color':'green'
        }, {
            name: data[2].key,
            data: data[2].data,
			'color':'rgba(51, 122, 164, 0.95)'
        }]
    });
});	
}

function highline1(id,xaxis,yaxis,data){
	//console.log(data);
$(function () {
    $(id).highcharts({
        title: {
            text: '',
            x: -20 //center
        },
		credits:{enabled:false},
        subtitle: {text: '',x: -20},
        xAxis: { categories: data[0].data	},
		
        yAxis: [{
			title: {
				text: yaxis
				},
				labels: {
					format:'{value} v',
					style: {color: 'red'},
					index:0
					},
				plotLines: [{
					value: 0,
					width: 1,
					color: 'red'
				}]					
				},{
					title:{
						text: yaxis
					},
					labels: {
						format:'{value} A',
						style:{color:'blue'},
						index:1
					},
					style: {
						color: Highcharts.getOptions().colors[1]
					},
					opposite: true
				}],
		
        tooltip: {valueSuffix: ''},
			
		legend:{backgroundColor: '#FFFFFF', floating: true, layout: 'vertical', verticalAlign: 'top',  x: 90, y: 60, shadow: true,align: 'right',verticalAlign: 'top',x:-10,y:10,
		    itemHoverStyle: {
                color: 'orange'
            }},	
         plotOptions: {
            series: {
                animation: {
                    duration: 2000
                }
            }
        }, 
        series: [{
            name: data[1].key,
			type: 'spline',
            data: data[1].data,
			tooltip: {
                valueSuffix: 'v'
            },
			'color':'green'
        }, {
            name: data[2].key,
			type: 'spline',
            data: data[2].data,
			tooltip: {
                valueSuffix: ' A'
            },
			'color':'rgba(51, 122, 164, 0.95)'
        }]
    });
});	
}

//----------------------------------------------------------------Dual Axis Highchart line -----------------------------
function highlinedualaxis(id,xaxis,yaxis,data){
	//console.log(data[1].key);
	//console.log(data[2].key);
	if(data[1].key == 'SOLARVOLTAGE'){
		TEXT1 = 'Voltage'
		SUFFIX1 = ' v'
	}
	if(data[1].key == 'SOLARPOWER'){
		TEXT1 = 'Power'
		SUFFIX1 = ' w'
	}	
	if(data[1].key == 'SOLARCURRENT'){
		TEXT1 = 'Current'
		SUFFIX1 = ' A'
	}
	if(data[2].key == 'SOLARVOLTAGE'){
		TEXT2 = 'Voltage'
		SUFFIX2 = ' v'
	}	
	if(data[2].key == 'SOLARCURRENT'){
		TEXT2 = 'Current'
		SUFFIX2 = ' A'
	}
	if(data[2].key == 'SOLARPOWER'){
		TEXT2 = 'Power'
		SUFFIX2 = ' w'
	}
	if(data[1].key == 'BATTERYVOLTAGE'){
		TEXT1 = 'Voltage'
		SUFFIX1 = ' v'
	}
	if(data[1].key == 'BATTERYPOWER'){
		TEXT1 = 'Power'
		SUFFIX1 = ' w'
	}
	if(data[1].key == 'BATTERYCURRENT'){
		TEXT1 = 'Current'
		SUFFIX1 = ' A'
	}
	if(data[2].key == 'BATTERYVOLTAGE'){
		TEXT2 = 'Voltage'
		SUFFIX2 = ' v'
	}
	if(data[2].key == 'BATTERYPOWER'){
		TEXT2 = 'Power'
		SUFFIX2 = ' w'
	}
	if(data[2].key == 'BATTERYCURRENT'){
		TEXT2 = 'Current'
		SUFFIX2 = ' A'
	}
	if(data[1].key == 'LOADVOLTAGE'){
		TEXT1 = 'Voltage'
		SUFFIX1 = ' v'
	}
	if(data[1].key == 'LOADPOWER'){
		TEXT1 = 'Power'
		SUFFIX1 = ' w'
	}
	if(data[1].key == 'LOADCURRENT'){
		TEXT1 = 'Current'
		SUFFIX1 = ' A'
	}
	if(data[1].key == 'LOADENERGY'){
		TEXT1 = 'Current'
		SUFFIX1 = ' A'
	}
	if(data[2].key == 'LOADVOLTAGE'){
		TEXT2 = 'Voltage'
		SUFFIX2 = ' v'
	}
	if(data[2].key == 'LOADPOWER'){
		TEXT2 = 'Power'
		SUFFIX2 = ' w'
	}
	if(data[2].key == 'LOADCURRENT'){
		TEXT2 = 'Current'
		SUFFIX2 = ' A'
	}
	if(data[2].key == 'LOADENERGY'){
		TEXT2 = 'Current'
		SUFFIX2 = ' A'
	}	
	
$(function () {
    $(id).highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {text: ''},
		credits:{enabled:false},
        subtitle: {text: ''},
		xAxis: { categories: data[0].data	},
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value} '+ SUFFIX2+'',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: TEXT2,
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
				plotLines: [{
					value: 0,
					width: 1,
					color: 'red'
				}]
        }, { // Secondary yAxis
            title: {
                text: TEXT1,
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} '+SUFFIX1+'',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
		legend:{backgroundColor: '#FFFFFF', floating: true, layout: 'vertical', verticalAlign: 'top',  x:-70, y:0, shadow: true,align: 'right',
		    itemHoverStyle: {
                color: 'orange'
            }},
        series: [{
            name:data[1].key,
            type: 'spline',
            yAxis: 1,
            data: data[1].data,
            tooltip: {
                valueSuffix:SUFFIX1
            }

        }, {
            name: data[2].key,
            type: 'spline',
            data: data[2].data,
            tooltip: {
                valueSuffix:SUFFIX2
            }
        }]
    });
});
}
/* function highlinesingleaxis(id,xaxis,yaxis,data){
	
$(function () {
    $(id).highcharts({
        rangeSelector: {
            selected: 1
        },		
		
        title: {
            text: '',
            x: -20 //center
        },
		credits:{enabled:false},
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: data[0].data
        },
        yAxis: {
			min:0,
            title: {
                text: 'Temperature (°C)'
            },
            plotLines: [{
                label: {
                    text: 'Surge',
                    x: 25
                },
				color: 'blue',		
                value: 153,
                width: 2,
                dashStyle: 'longdashdot'
            },{
                label: {
                    text: 'Sag',
                    x: 25
                },
				color: 'blue',		
                value: 75,
                width: 2,
                dashStyle: 'longdashdot'							
			}]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
		{
			threshold: 75,
			negativeColor: 'red',
			positiveColor: 'red',
			color: 'green',
			name:'Normal Range',
            data: data[1].data
        },{
			threshold: 153,
			negativeColor: 'transparent',
			color: 'red',
			name:'Surge | Sag Range',
            data: data[1].data			
		} ]
    });
});
} */

function highlinesingleaxis(id,xaxis,yaxis,data){
	
$(function () {
	
	
        var figuress = data[1].data;
        var figures = data[2].data,length,
            d = [];
			d1=[];
		window.last = figures.length - 1
		window.lstt = data[1].data[last]

        $.each(figures, function (i, figure) {
            if (figures[i] == 0) {
                d.push({y: figuress[i], fillColor: '#FFFFFF', lineWidth: 1,lineColor:'rgb(124, 181, 236)', radius:3});  //fillColor:'#FFFFFF'
            }
			else if (figures[i] != 0){
               d.push({y: figuress[i], fillColor: '#CF2A0E',radius:4});
            }	
        });	
	
	
    $(id).highcharts({
        chart: {type: 'spline',marginBottom: 50},	
        rangeSelector: {
            selected: 1
        },		
		
        title: {
            text: '',
            x: -20 //center
        },
		credits:{enabled:false},
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: data[0].data
        },
        yAxis: {
			gridLineDashStyle: 'dash',
			min:0,
            title: {
                text: 'Values '
            }
            
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
			enabled:false,
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0
        
        },
       		
        series: [
		{
			name:'Normal Range',
            data: d,
			color:'rgb(58, 83, 155)'
        }	
		]
    });
});
} 




//-----------------------------------------Highchart with data labels --------------------------------------------------------
function highchartlinewithdatalabels(id,xaxis,yaxis,data){
	
		
	$(function () {
		
        var figures = data[1].data,length,
            d = [];
		window.last = figures.length - 1
		window.lstt = data[1].data[last]
		
        $.each(figures, function (i, figure) {

			
            if (figures[i] == window.lstt) {
               
                d.push({y: figure, fillColor: 'white',lineWidth: 2,lineColor:'rgba(51, 122, 164, 0.95)'});
            }
			else if (figures[i] != window.lstt){
                d.push({y: figure,fillColor: 'white',lineWidth: 2,lineColor:'rgba(51, 122, 164, 0.95)'});
            }	
        });
   	
				
		$(id).highcharts({
			tooltip: {crosshairs: true},			
			chart: { type: 'spline'},
			legend: {enabled: false},			
			chart: {type: 'line'},
			credits:{enabled:false},
			title: {text: ''},
			subtitle: {text: ''},
			xAxis: {
				labels: { rotation: -25, },				
				categories: data[0].data,
            title: {
                text: ''
                
            }				
			},
			yAxis: {
				gridLineDashStyle: 'dash',
				offset: -10,
				labels: {
                format: '{value}' + '',
                style: {  color: 'rgba(0, 0, 0, 0.77);' }
				},
				title: {
					text:yaxis,
					  style: {  color: 'rgba(0, 0, 0, 0.77);' }
				}
			},
			plotOptions: {
            series: {
            marker: {radius: 3},				
                dataLabels: {                  
					color:'rgba(0, 0, 0, 0.5)',
                    borderRadius: 2,               
                    backgroundColor: 'transparent',
                    borderWidth: 1,                    				
                },
				
            },				
				line: {
					dataLabels: {
						/* enabled: false */
					},
					enableMouseTracking: true
				}
			},
			series: [{
				name: 'Panel Output',
				data: d,
				color:'#309ad8',
				tooltip: {
					valueSuffix:'%'
				}				
			}]
		});
	});
}


function battery_cycle_efficiency(id,xaxis,yaxis,data){
	//console.log(data[1].data[4]);
	
	
	
	$(function () {
		
		//var last = arr[data[1].data.length - 1]
        var figures = data[1].data,length,
            d = [];
		window.last = figures.length - 1
		window.lstt = data[1].data[last]
		//console.log(window.last);
		//console.log(window.lstt);
        $.each(figures, function (i, figure) {

			//console.log(window.lstt);
			//console.log(figures[i]);
            if (figures[i] == window.lstt) {
                /* d.push({y: figure, fillColor: '#E36159',radius: 4}); */
                d.push({y: figure, fillColor: 'white',lineWidth: 2,lineColor:'#5647B1',radius: 3});
            }
			else if (figures[i] != window.lstt){
                d.push({y: figure, fillColor: 'white',lineWidth: 2,lineColor:'#5647B1',radius: 3});
            }	
        });
   	
				
		$(id).highcharts({
			chart: {type: 'spline',marginBottom:0},
			legend: {enabled: false},			
			chart: {type: 'line'},
			credits:{enabled:false},
			title: {text: ''},
			subtitle: {text: ''},
			xAxis: {
				labels: { rotation: 0, },				
				categories: data[0].data
			},
			yAxis: {
				gridLineDashStyle: 'dash',
				offset: -10,
				labels: {
                format: '{value}' + '',
                style: {  color: 'rgba(0, 0, 0, 0.77);' }
				},
				title: {
					text:yaxis,
					  style: {  color: 'rgba(0, 0, 0, 0.77);' }
				}
			},
			plotOptions: {
            series: {
            marker: {radius: 3},				
                dataLabels: {
                    /* enabled: true, */
					/* format: '{y}%', */
					color:'rgba(0, 0, 0, 0.5)',
                    borderRadius: 2,
                    /* backgroundColor: 'rgba(252, 255, 197, 0.7)', */
                    backgroundColor: 'transparent',
                    borderWidth: 1,
                    /* borderColor: '#AAA', */
                    				
                },
				
            },				
				line: {
					dataLabels: {
						/* enabled: true */
					},
					enableMouseTracking: true
				}
			},
			series: [{
				name: 'Efficiency/Day',
				data: d,
				color:'#7266BA',
				tooltip: {
					valueSuffix:'%'
				}				
			}]
		});
	});
}


function highchartgenconratee(id,xaxis,yaxis,data){
	//console.log(data);
	$(function () {
    $(id).highcharts({
		chart: {type: 'spline',marginBottom:37},
		legend: {
			enabled: false
		},	
		credits:{enabled:false},			
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: data[0].data
        },
        yAxis: {
			gridLineDashStyle: 'Dash',
			offset: -10,
			tickInterval: 0.5,
            title: {
                text: 'Energy (Kw/H)',
				style: {  color: 'rgba(0, 0, 0, 0.77)', fontSize:'12px'},
            },
            labels: {
                format: '{value}',
                style: {  color: 'rgba(0, 0, 0, 0.77);' }
				},			
            plotLines: [{
                value: 0,
                width: 1,
            }]
        },
		plotOptions: {series: {}},			
        tooltip: {
            valueSuffix: ' Kw/H'
        },
        series: [{
            name: 'Generation',
            data: data[1].data,
			marker: {radius: 2,fillColor: 'white',lineWidth: 2,lineColor:'rgb(48, 154, 216)',}
			
        }, {
            name: 'Consumption',
            data: data[2].data,
			color: 'rgba(51, 51, 51, 0.65)',
			marker: {radius: 2,fillColor: 'white',lineWidth: 2,lineColor:'rgba(51, 51, 51, 0.65)',}
        }]
    });
});
}
//-------------------------Highchart Line plus Bar chart------------------------------------

function highchartlineplusbar(id,xaxis,yaxis,data){
	
	
	$(function () {	
    $(id).highcharts({
        chart: {
            zoomType: 'xy',
			marginBottom: 25
        },
		credits:{enabled:false},	
        title: {
            text: ''
        },		
        subtitle: {
            text: 'Log and New Log occurance',
			 floating: false,
            style: {color: '#999999',}			 
        },
        xAxis: [{
            categories:data[0].data,
            crosshair: true,
			labels: { rotation: -10, }
        }],
		
       plotOptions: {
            series: {
                animation: {
                    duration: 2000,
					easing: 'easeOutBounce'				  
                },				
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
							var d = new Date();			//Current Date
							var n = d.getFullYear();	//Current Year
                            window.a = this.category;	//Graph Date Ex:Feb-26
							window.aa = window.a +'-'+n;	//add year to graph date Ex:Feb-26-2016
							ab = new Date(window.aa)		//Get standard date_time from Feb-26-2016
							window.ac = ab.getFullYear()+'-' + ((ab.getMonth()+1) < 10 ? '0'+(ab.getMonth()+1) : (ab.getMonth()+1)) + '-'+ ((ab.getDate()) < 10 ? '0'+(ab.getDate()) : (ab.getDate()) );
					
							console.log(ac);
							log();
							
							
                        }
                    }
                },
							
            }
           			
        }, 		
        yAxis: [{ 
		 gridLineDashStyle: 'dash',
            labels: {
				enabled:true,
                format: '{value}',
                style: {color: '#1684c6'}
            },
            title: {
                text: ' # New Log ',
                style: {color: '#1684c6'}
            }
        }, { 
		 gridLineDashStyle: 'dash',
            title: {
                text: ' # Log ',
                style: {color:'#9E9E9E'}
            },
            labels: {
				enabled:true,
                format: '{value}',
                style: { color: '#9E9E9E'}
            },
            opposite: true
        }],
        tooltip: {shared: true},
       
        series: [
		{		
			showInLegend: false,
            name: 'Log Count',
			/* color:'rgb(61, 154, 135)', */
			color:'#d1dade',
            type: 'column',
            yAxis: 1,
            data: data[1].data,
            tooltip: {
                valueSuffix: ''
            }

        }, {
			showInLegend: false,
            name: 'New Log Count',
			/* color:'#C7A610', */
			color:'#1684c6',
            type: 'spline',
            data: data[2].data,
			marker: {radius: 3,fillColor: 'white',lineWidth: 2,lineColor:'#1684c6',},
            tooltip: {
                valueSuffix: ''
            }
        }]
    });
});
}
function log(){
	console.log(window.ac);
	$.ajax({
		type:'GET',
		url:'/health_moniter/getoverviewpagedata',
		data:{'click_on_bar_date':window.ac},
		success:function(data){
			//console.log(data);
			click_bar(data);
			//console.log(result['date']);
			
			/* highchartclicklog=[
			{
				key:'X-axis',
				data:data['date']
			},
			{
				key:'Efficiency',
				data:data['eff']
			}
			];	
			highchartlinewithdatalabels('#logsecondgraph','x','Efficiency',highchartclicklog)	 */		
			//console.log(highchartclicklog);
			
			
			
		}
		
	});
}

function highchartparametergraph(id,xaxis,yaxis,data){
	$(function () {
    $(id).highcharts({
		credits:{enabled:false},
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: data[0].data,
			minTickInterval: 10
        },
        yAxis: {
			gridLineDashStyle: 'dash',
            title: {
                text: 'Values'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
		legend:{enabled:false,backgroundColor: '#FFFFFF', floating: true, layout: 'vertical', verticalAlign: 'top',  x:-70, y:0, shadow: true,align: 'right',
		    itemHoverStyle: {
                color: 'orange'
            }},
        series: [{
            name: '',
            data: data[1].data
        }]
    });
});
}


function time_ago(time){

switch (typeof time) {
    case 'number': break;
    case 'string': time = +new Date(time); break;
    case 'object': if (time.constructor === Date) time = time.getTime(); break;
    default: time = +new Date();
}
var time_formats = [
    [60, 'seconds', 1], // 60
    [120, '1 minute ago', '1 minute from now'], // 60*2
    [3600, 'minutes', 60], // 60*60, 60
    [7200, '1 hour ago', '1 hour from now'], // 60*60*2
    [86400, 'hours', 3600], // 60*60*24, 60*60
    [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
    [604800, 'days', 86400], // 60*60*24*7, 60*60*24
    [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
    [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
    [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
    [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
    [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
    [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
    [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
    [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
];
var seconds = (+new Date() - time) / 1000,
    token = 'ago', list_choice = 1;

if (seconds == 0) {
    return 'Just now'
}
if (seconds < 0) {
    seconds = Math.abs(seconds);
    token = 'from now';
    list_choice = 2;
}
var i = 0, format;
while (format = time_formats[i++])
    if (seconds < format[0]) {
        if (typeof format[2] == 'string')
            return format[list_choice];
        else
            return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
    }
return time;
}

//--------------------------------------------------------------------------------------------------
function highchart_column_chart(id,xvalue,yvalue,data){
	//console.log(data);
	
Math.easeOutBounce = function (pos) {
    if ((pos) < (1 / 2.75)) {
        return (7.5625 * pos * pos);
    }
    if (pos < (2 / 2.75)) {
        return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
    }
    if (pos < (2.5 / 2.75)) {
        return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
    }
    return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
};	
	
	$(function () {
    $(id).highcharts({
        chart: {
            type: 'bar',
			marginBottom: 50
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: data[0].data,
            title: {
                text: ""
            }
        },
        yAxis: {
			tickInterval: 1,
			gridLineDashStyle: 'dash',
            min: 0,
            title: {
                text: 'Frequency',
                
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' '
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            },
            series: {
                animation: {
                    duration: 2000,
					 easing: 'easeOutBounce',
					  
                }
            }			
        },
        /* plotOptions: {

            series: {
                animation: {
                    duration: 2000,
					 easing: 'easeOutBounce',
					  
                }
            }
        }, */		
        legend: {
			enabled: false,
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Year 1800',
            data: data[1].data,
			/* color:'#d1dade', */
        }]
    });
});
}
//--------------------------------------------Click on Bar Function ---------------------------------------
function click_bar(data){
	//console.log(data['bartime'][1]);
	
	Math.easeOutBounce = function (pos) {
		if ((pos) < (1 / 2.75)) {
			return (7.5625 * pos * pos);
		}
		if (pos < (2 / 2.75)) {
			return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
		}
		if (pos < (2.5 / 2.75)) {
			return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
		}
		return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
	};	
	
	$(function () {
		
		
		d = [];
		counter=0;
		window.s = 0;
		for(i=0;i<=23;i++){
			
			if (data['bartime'][window.s] != counter) {
				
				d.push({y: -1 });
			}
			else if(data['bartime'][window.s] == counter){
				d.push({y:data['barvalue'][window.s]});
				window.s = window.s +1;
			}
			
			counter = counter+1;
		}
		
		
		
		$('#click_on_bar').highcharts({

			chart: {
				type: 'column',
				marginBottom: 45
			},
					credits:{
			enabled:false
			},
			title: {
				text: ''
			},

			xAxis: {
				categories: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
				title:{
					text:'Time (Hours)'
				}
			},

			yAxis: {
				gridLineDashStyle: 'dash',
				allowDecimals: false,
				min: 0,
				title: {
					text: 'Frequency of occurance'
				},
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            },				
			},

			tooltip: {
				formatter: function () {
					return '<b>' + this.x + '</b><br/>' +
						this.series.name + ': ' + this.y + '<br/>' +
						'Total: ' + this.point.stackTotal;
				}
			},

			plotOptions: {
				column: {
					stacking: 'normal'
				},
				series: {
					animation: {
						duration: 2000,
						easing: 'easeOutBounce'
					}
				}				
			},
			legend: {
				enabled: false,
			},	
			series: [{
				
				data: d,
				stack: 'male',
				color:'#d1dade'
			}]
		});
	});	
}





