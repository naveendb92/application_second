layout();
var variable=[];
$(document).foundation({
    accordion: {
      callback : function (accordion) {
        console.log(accordion);
      }
    }
});
fun_left_menu(left_menu);
function fun_left_menu(menu){
	for(var i=0;i<menu.length;i++){
		var a="";
		var url = $(location).attr('href').split("/");
		if(menu[i].link == url[4]){
			a = 'class="active"';
		}else if(menu[i].menu.length != 0){
			
			for(var j=0;j < menu[i].menu.length;j++){
				if(menu[i].menu[j].link == url[4]){
					a = 'class="active"';
				}
			}
		}
		$('.left-menu ul').append(
			$('<li link="'+menu[i].link+'" title="'+menu[i].key+'" '+a+'>').append(
				$('<i class="fa fa-'+menu[i].img+'"></i><span>'+menu[i].key+'</span>')
		));
	}
}
function fun_left_menu_extend(menu){
	$('.menu-extend ul').html("");
	for(var i=0;i<menu.length;i++){
		$('.menu-extend ul').append($('<li link="'+menu[i].link+'">').append(menu[i].key));
		$(".menu-extend ul li").click(function(){
			window.location.href = $(this).attr("link");
		});
	}
}
function layout(){
	$(".main").height($(window).height() - 81);
	$(".menu-extend").height($(window).height() - 81);
	$(".left-menu").height($(window).height() - 81);
	$(".main-container").height($(window).height());
}

$(window).resize(function(){
	layout();
});

$(".left-menu ul li").mouseover(function(){
	$(".menu-extend").html("");
	var title = $(this).attr("title");
	$(".menu-extend").append($('<div class="header">').html('<i class="fa fa-'+left_menu[$(this).index()].img+'"></i>'+title));
	$(".menu-extend").append($('<ul>'));
	if(left_menu[$(this).index()].menu.length != 0){
		$(".menu-extend").css("display","inline");
		fun_left_menu_extend(left_menu[$(this).index()].menu);
	}
});

$(".left-menu ul li").mouseout(function(){
	$(".menu-extend").css("display","none");
});
$(".menu-extend").mouseover(function(){
	$(".menu-extend").css("display","inline");
});
$(".menu-extend").mouseout(function(){
	$(".menu-extend").css("display","none");
});

$(".left-menu ul li").click(function(){
	$(".left-menu ul li").removeClass("active");
	$(this).addClass("active");
	if($(this).attr("link") != "")
		window.location.href = $(this).attr("link");
});
function datatable_settings(tableid,rows){
	var table = $(tableid).DataTable({
					'iDisplayLength': rows,
					/*"aoColumnDefs" : [ {
						'bSortable' : false,
						 'aTargets' : [ 0,3 ]
						} ],*/
						
					"oLanguage": {
						"sZeroRecords": "No Record Found"
					}
				});
	$('.dataTables_filter input').attr("placeholder", "Search");
		
}

function table_column_clickable(tableid,column_num){
	
	$(tableid+' tbody tr td:nth-child('+column_num+')').css({'cursor':'pointer','color':'#0078a0'});
	var column_value =$(tableid+' tbody tr td:nth-child('+column_num+')').text();
	$(document).delegate('#table-sites tbody tr td','click',function(){
		var tableid = $(this).closest('tr').css({'background':'red'});
		
	});

}

