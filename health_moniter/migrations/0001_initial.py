# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='master',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location', models.CharField(max_length=100)),
                ('softwarerevision', models.CharField(max_length=100)),
                ('date', models.DateField()),
                ('time', models.TimeField()),
                ('solarvoltage', models.DecimalField(max_digits=9, decimal_places=2)),
                ('solarcurrent', models.DecimalField(max_digits=9, decimal_places=2)),
                ('solarpower', models.DecimalField(max_digits=9, decimal_places=2)),
                ('solarenergy', models.DecimalField(max_digits=9, decimal_places=2)),
                ('batteryvoltage', models.DecimalField(max_digits=9, decimal_places=2)),
                ('batterycurrent', models.DecimalField(max_digits=9, decimal_places=2)),
                ('batterypower', models.DecimalField(max_digits=9, decimal_places=2)),
                ('batteryenergypositive', models.DecimalField(max_digits=9, decimal_places=2)),
                ('batteryenergynegative', models.DecimalField(max_digits=9, decimal_places=2)),
                ('loadvoltage', models.DecimalField(max_digits=9, decimal_places=2)),
                ('loadcurrent1', models.DecimalField(max_digits=9, decimal_places=2)),
                ('loadcurrent2', models.DecimalField(max_digits=9, decimal_places=2)),
                ('loadpower1', models.DecimalField(max_digits=9, decimal_places=2)),
                ('loadpower2', models.DecimalField(max_digits=9, decimal_places=2)),
                ('loadenergy1', models.DecimalField(max_digits=9, decimal_places=2)),
                ('loadenergy2', models.DecimalField(max_digits=9, decimal_places=2)),
                ('accurrent', models.CharField(max_length=100)),
                ('acstatus', models.CharField(max_length=100)),
                ('irradiance', models.DecimalField(max_digits=9, decimal_places=2)),
                ('room_temperature', models.DecimalField(max_digits=9, decimal_places=2)),
                ('charge_mode', models.CharField(max_length=100)),
                ('inverter_input_energy', models.DecimalField(max_digits=9, decimal_places=2)),
                ('inverter_output_energy', models.DecimalField(max_digits=9, decimal_places=2)),
                ('inverter_input_power', models.DecimalField(max_digits=9, decimal_places=2)),
                ('inverter_output_power', models.DecimalField(max_digits=9, decimal_places=2)),
            ],
        ),
    ]
