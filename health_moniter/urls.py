from django.conf.urls import patterns,url,include
from django.contrib import admin
from . import views


urlpatterns = patterns('',		             			
	url(r'^overview$', views.overview, name='overview'),     
	url(r'^gotopage2$', views.gotopage2, name='gotopage2'),     
	url(r'^log_overview$', views.log_overview, name='log_overview'),
	url(r'^generation$', views.generation, name='generation'),
	url(r'^consumption$', views.consumption, name='consumption'),
	url(r'^Site_Visit_Log$', views.Site_Visit_Log, name='Site_Visit_Log'),
	url(r'^Create_Log$', views.Create_Log, name='Create_Log'),
	url(r'^Review_Log$', views.Review_Log, name='Review_Log'),
	url(r'^getoverviewpagedata$', views.getoverviewpagedata, name='getoverviewpagedata'),
	url(r'^getgenerationpagedata$', views.getgenerationpagedata, name='getgenerationpagedata'),
	url(r'^getconsumptionpagedata$', views.getconsumptionpagedata, name='getconsumptionpagedata'),
	url(r'^getstoragepagedata$', views.getstoragepagedata, name='getstoragepagedata'),
	url(r'^getrefreshtime$', views.getrefreshtime, name='getrefreshtime'),
	url(r'^getbestworstcasedate$', views.getbestworstcasedate, name='getbestworstcasedate'),
	url(r'^getcustom_case_result$', views.getcustom_case_result, name='getcustom_case_result'),   
	url(r'^getstoragebatterypagedata$', views.getstoragebatterypagedata, name='getstoragebatterypagedata'),   
	url(r'^getpage2loaddata$', views.getpage2loaddata, name='getpage2loaddata'),    
	url(r'^getstoragebatterypagedataweekly$', views.getstoragebatterypagedataweekly, name='getstoragebatterypagedataweekly'),    
	url(r'^getsitevisitlogdata$', views.getsitevisitlogdata, name='getsitevisitlogdata'),    
	url(r'^getcreatelogdata$', views.getcreatelogdata, name='getcreatelogdata'),    
	url(r'^getreviewlogdata$', views.getreviewlogdata, name='getreviewlogdata'),    
	url(r'^getreviewlogdeatilsdata$', views.getreviewlogdeatilsdata, name='getreviewlogdeatilsdata'),    
	url(r'^getlogoverviewdata$', views.getlogoverviewdata, name='getlogoverviewdata'),    
)